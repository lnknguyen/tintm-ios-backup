//
//  LibraryViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/30/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography
import Bond
import WebKit
import RealmSwift

class LibraryViewController: UIViewController{
    
    var router : ArticleViewRouter?
    
    var fileName : String?
    var webView = UIWebView()
    let backButton = UIButton(frame: CGRect(x: 0,y: 0,width: 25,height: 25))
    let bundle = Bundle.main
    var path = ""
    
    typealias ArticleViewPresenter = TextPresentable & ImagePresentable
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init(fileName: String){
        self.init(nibName: nil, bundle: nil)
        self.fileName = fileName
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        path =  bundle.path(forResource: "default", ofType: "css")!
        configureSubviews()
        constrainSubviews()
        let url = Bundle.main.url(forResource: fileName, withExtension:"html")
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    
    func constrainSubviews(){
        view.addSubview(webView)
        constrain(webView) { (webView) in
            webView.top == (webView.superview?.top)!
            webView.left == (webView.superview?.left)! + 5
            webView.right == (webView.superview?.right)! - 5
            webView.bottom == (webView.superview?.bottom)! - 5
        }
    }
    
    fileprivate func configureSubviews(){
        view.backgroundColor = UIColor.clear
        if #available(iOS 9.0, *) {
            webView.allowsLinkPreview = true
        } else {
            // Fallback on earlier versions
        }
        
        //Enable zooming in webview
        var scriptContent = "var meta = document.createElement('meta');"
        scriptContent += "meta.name='viewport';"
        scriptContent += "meta.content='width=device-width,initial-scale=0.3, maximum-scale=2.0, user-scalable=2.0';"
        scriptContent += "document.getElementsByTagName('head')[0].appendChild(meta);"
        
        
        webView = UIWebView(frame: CGRect.zero)
        
        webView.scrollView.minimumZoomScale = 1.0
        webView.scrollView.maximumZoomScale = 2.0
        
        webView.scrollView.bounces = false
        
        
    }
    
}
