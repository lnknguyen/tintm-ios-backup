//
//  NewsSourceViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 12/4/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//

import UIKit
import CoreData
import Google
import Kingfisher

class NewsSourceViewController : UITableViewController {
    var viewModel = NewsSourceViewModel()
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
        tableView.register(NewsSourceCell.self, forCellReuseIdentifier: NEWS_SOURCE_CELL_ID)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil,bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(){
        self.init(style: .grouped)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        viewModel.publishDataSource()
    }
    
    //MARK: Delegate and data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NEWS_SOURCE_CELL_ID) as! NewsSourceCell
        let entity = viewModel.dataSource[indexPath.row]
        
        cell.imageView?.kf.setImage(with: URL(string: entity.icon), placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) in
            cell.setNeedsLayout()
        })
        cell.textLabel!.text = entity.alias
        cell.accessoryType = entity.status ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var entity = viewModel.dataSource[indexPath.row]
        if (entity.type == "Major" && entity.status == true){
            let alert = UIAlertController(title: "Cảnh báo",
                                          message: "Bỏ chọn nguồn này sẽ làm nguồn tin bị hạn chế.Bạn có chắc chắn muốn bỏ chọn không?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                entity.status = !entity.status
                self.viewModel.dataSource[indexPath.row] = entity
                
                //self.viewModel.writeToJSONFile()
                self.tableView.reloadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            entity.status = !entity.status
            viewModel.dataSource[indexPath.row] = entity
            
            //viewModel.writeToJSONFile()
            self.tableView.reloadData()
        }
        
        
        
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0{
            let footerView = UIView(frame: CGRect(x: 0,y: 0,width: APP_WIDTH,height: 50))
            let saveButton = UIButton(frame: CGRect(x: 0,y: 0,width: APP_WIDTH,height: 50))
            saveButton.backgroundColor = UIColor.appColor()
            saveButton.layer.cornerRadius = 5
            saveButton.setTitle("Lưu", for: UIControlState())
            saveButton.titleLabel?.textAlignment = .center
            saveButton.titleLabel!.font = UIFont.appBoldFont(25.0)
            saveButton.titleLabel?.textColor = UIColor.white
            saveButton.addTarget(self, action: #selector(NewsSourceViewController.saveButtonHit), for: UIControlEvents.touchUpInside)
            footerView.addSubview(saveButton)
            footerView.backgroundColor = UIColor.red
            
            return footerView
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func saveButtonHit(){
        viewModel.writeToJSONFile()
        AlertViewManager.sharedInstance.showSuccess(InfoCode.bookmarked)
        let dict: [String:AnyObject] = ["data":viewModel]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "newsSourceChanged"), object: nil,
                                                                  userInfo: dict)
        let firstTimeLoggedIn = UserDefaults.standard.string(forKey: "firstTimeLoggedIn")
        
        if (firstTimeLoggedIn == "true"){
            UserDefaults.standard.set("false", forKey: "firstTimeLoggedIn")
            let rootVc = RootViewController()
            /*
            let rootNavVc = UINavigationController(rootViewController: rootVc)
            rootNavVc.navigationBarHidden = true
            rootNavVc.navigationBar.translucent = true
            rootNavVc.navigationBar.barStyle = .Black
            rootNavVc.customizeNavigationBar()
             UIApplication.sharedApplication().keyWindow?.rootViewController = rootNavVc
            UIApplication.sharedApplication().keyWindow?.makeKeyAndVisible()
            */
            self.navigationController?.pushViewController(rootVc, animated: true);
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
}


