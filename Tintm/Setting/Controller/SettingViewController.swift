//
//  SettingViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import CoreData
import Google

class SettingViewController : UITableViewController {
    var viewModel : SettingViewModel?
    var router: SettingViewRouter?
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
        tableView.register(SettingCell.self, forCellReuseIdentifier: SETTING_CELL_ID)
        
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil,bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init(){
        self.init(style: .grouped)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: Delegate and data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel?.menu[section].count)!
    }
    
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: SETTING_CELL_ID) as! SettingCell
        
        if indexPath != IndexPath(row: 0, section: 1){
            
        } else {
            let sw = UISwitch()
            cell.accessoryView = sw
            sw.onTintColor = UIColor.appColor()
            sw.isOn = appDataObject!.allowOffline
            
            _ = sw.reactive.isOn.observeNext(with: {[unowned self] (value) in
                self.viewModel?.switchHandler(value)
            })
        }
        cell.textLabel?.text = viewModel?.menu[section][row]
        cell.configureWithPresenter(viewModel!)
        return cell
    }
    
    //Rework later : Add more clarification ( replace number with enum ? )
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Analytics
        let tracker = GAI.sharedInstance().defaultTracker
        
        switch indexPath.section{
        case 0:
            switch indexPath.row{
            case 0:
                let event = GAIDictionaryBuilder.createEvent(
                    withCategory: "setting_event",
                    action: "tapped",
                    label: "goto_website",
                    value: nil).build()
                tracker?.send(event as! [AnyHashable: Any])
                router!.pushToWebsiteView()
            case 1:
                router?.pushToNewsSourceConfigurationView()
                
            case 2:
                let event = GAIDictionaryBuilder.createEvent(
                    withCategory: "setting_event",
                    action: "tapped",
                    label: "goto_info",
                    value: nil).build()
                tracker?.send(event as! [AnyHashable: Any])
                router!.pushToInfoView()
            case 3:
                let event = GAIDictionaryBuilder.createEvent(
                    withCategory: "setting_event",
                    action: "tapped",
                    label: "goto_mail",
                    value: nil).build()
                tracker?.send(event as! [AnyHashable: Any])
                router?.pushToMailView()
                
            default:
                return
            }
        case 1:
            switch indexPath.row {
            case 1:
                router?.presentDeleteOfflineNews()
            default:
                break
            }
        default:
            return
        }
    }
    
    //Header and footer
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Sản phẩm"
        case 1:
            return "Chế độ offline"
        default:
            break
        }
        return ""
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let label = UILabel(frame: CGRect(x: 0,y: 20,width: APP_WIDTH,height: 40))
            
            label.attributedText = NSAttributedString(string: "Version: 1.0.0", attributes: [NSForegroundColorAttributeName: UIColor.appColor(), NSFontAttributeName: UIFont.appRegularFont(16)])
            label.textAlignment = .center
            label.sizeToFit()
            return label
        default:
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 50.0
        default:
            return 0.0
        }
    }
    
    
}

