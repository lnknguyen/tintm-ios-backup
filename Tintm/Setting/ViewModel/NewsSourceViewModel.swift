//
//  NewsSourceViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 12/5/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import ObjectMapper

class NewsSourceViewModel: NSObject {
    var dataSource: [NewsSourceEntity] = [];
    
    func publishDataSource(){
        guard let content = readFromJSONFile() else {return}
        guard let data = Mapper<NewsSourceEntity>().mapArray(JSONString: content) else {
            print("cant parse from file")
            return
        }
        dataSource.append(contentsOf: data)
    }
    //Local json operations
    func readFromJSONFile()-> String?{
        guard let path = Bundle.main.path(forResource: "source-info", ofType: "json")
            else {return nil}
        do {
            let content  = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
           
            return content
            
        } catch {
            print("error")
            
        }
        return nil
    }
    
    func writeToJSONFile(){
        var jsonData : Data!
        var jsonString : String!
        do{
            let compatibleArray = dataSource.map({ (model)in
                return ["NewsSourceId": model.id , "Favicon": model.icon, "Alias": model.alias, "Status": model.status, "Type":model.type]
            })
            jsonData = try JSONSerialization.data(withJSONObject: compatibleArray, options: JSONSerialization.WritingOptions.prettyPrinted)
            jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            
            
        }catch{
            print("error parsing")
        }
        guard let path = Bundle.main.path(forResource: "source-info", ofType: "json")
            else {return}
        let url = URL(fileURLWithPath: path)
        
        do{
            try jsonString.write(to: url, atomically: true, encoding: String.Encoding.utf8)
        }catch {
            print("Error print to file")
        }
    }
}

extension NewsSourceViewModel : TextPresentable {
    var textColor: UIColor {
        return UIColor.appColor()
    }
    
    var headingFont: UIFont {
        return UIFont.appRegularFont(16.0)
    }
}
