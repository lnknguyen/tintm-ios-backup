//
//  SettingViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/30/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import RealmSwift

struct SettingViewModel {
    let section = ["Sản phẩm"]
    let menu : [[String]] = [["Đến Website","Chọn nguồn tin","Thông tin","Góp ý & Phản hồi lỗi"],["Cho phép lưu offline","Xoá tin lưu offline"]]
    let realm = try! Realm()
    
    func switchHandler(_ value: Bool){
        try! self.realm.write{
            appDataObject!.allowOffline = value
        }
    }
    
}

extension SettingViewModel : TextPresentable {
    var textColor: UIColor {
        return UIColor.appColor()
    }
    
    var headingFont: UIFont {
        return UIFont.appRegularFont(16.0)
    }
}
