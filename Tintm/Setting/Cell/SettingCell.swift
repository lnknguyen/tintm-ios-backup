//
//  SettingCell.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/30/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

class SettingCell : UITableViewCell {
    typealias Presenter = TextPresentable
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: SETTING_CELL_ID)
         self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureWithPresenter(_ presenter: Presenter){
        textLabel?.font = presenter.headingFont
        textLabel?.textColor = presenter.textColor
        
    }
}
