//
//  PageViewViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Bond

protocol PageViewAdapter: class {
    func successHandler(_ ids: [Int])
    func failureHandler(_ err: Error)
}

class PageViewViewModel {
    var id : Int = 0
    
    
    //var idList : Observable<[Int]>
    var idList = [Int]()
    var vcArray = [ArticleViewController]()
    weak var delegate : PageViewAdapter?
    
    init(id: Int = 0){
        self.id = id
        //idList = Observable([])
        let vc = ArticleViewController()
        let vm = ArticleViewModel(id: id)
        vc.viewModel = vm
        vcArray.append(vc)
    }
    
    func createVcArray(){
        for id in idList {
            let vc = ArticleViewController()
            let vm = ArticleViewModel(id: id)
            vc.viewModel = vm
            //vc.bindViewModel()
            vcArray.append(vc)
        }
        
    }
    
    
    func loadRelatedFeeds(){
        NetworkManager.sharedInstance.getRelatedFeeds(id,limit: 1) { (res) in
            switch res.status {
            case .success:
                if let result = res.res{
                   
                    //Extract id from entity
                    let ids = result.map({ (entity) -> Int in
                        return entity.id
                    })
                    //Add to ids list
                    self.idList = ids
                    self.delegate?.successHandler(ids)
                }
            case .failure:
                if let err = res.error{
                    self.delegate?.failureHandler(err)
                }
            }
            
        }
    }
    
    
    
}
