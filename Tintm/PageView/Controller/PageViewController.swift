//
//  PageViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Bond
import Google
import ReactiveKit

class PageViewController: UIPageViewController {
    var viewModel : PageViewViewModel
    var currentViewModel : Observable<ArticleViewModel>
    var currentVc : Observable<ArticleViewController>
    let bookmarkButton = UIButton(frame: CGRect(x: 0,y: 0,width: 35,height: 35))
    let shareButton = UIButton(frame: CGRect(x: 0,y: 0,width: 35,height: 35))
    let realmManager = RealmCommandManager()
    let pageControl = UIPageControl()
    
    //Images
    let nonBookmarkedImg = UIImage(named: ImageName.DisabledBookmarkIcon.description)
    let bookmarkedImg = UIImage(named: ImageName.EnabledBookmarkIcon.description)
    
    //Current url
    var url : URL?
    
    //Observer
    var bookmarkObserver: SimpleDisposable?
    var shareObserver: SimpleDisposable?
    //analytics
    let tracker = GAI.sharedInstance().defaultTracker
    
    override init(transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : Any]?) {
        
        currentViewModel = Observable(ArticleViewModel())
        currentVc = Observable(ArticleViewController())
        viewModel = PageViewViewModel()
        
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
        self.delegate = self
    }
    
    convenience init(){
        self.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        dataSource = self
        navigationController?.navigationBar.isTranslucent = false
        viewModel.delegate = self
        bindViewModel()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
     func bindViewModel(){
        viewModel.createVcArray()
        configureNavigationBar()
        
        setupPageControl(viewModel.vcArray.count)
        if let firstVc = viewModel.vcArray.first {
            let router = ArticleViewRouter(vc: firstVc)
            firstVc.router = router
            setViewControllers([firstVc], direction: .forward, animated: true, completion: nil)
            firstVc.bindViewModel()
            observeChangeInValue(firstVc.viewModel)
        }
        
        
    }
    
    internal func setupPageControl(_ size: Int) {
        
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.backgroundColor = UIColor.clear
        pageControl.numberOfPages = size
        pageControl.currentPage = 0
        self.navigationItem.titleView = pageControl
    
    }
    
    fileprivate func configureNavigationBar(){
        self.navigationController?.isNavigationBarHidden = false
        
        let backbutton = UIButton(frame: CGRect(x: 0,y: 0,width: 50,height: 35))
        
        backbutton.imageEdgeInsets = UIEdgeInsetsMake(5, -15, 5,35)
        
        backbutton.setImage(UIImage(named: ImageName.BackIcon.description), for: UIControlState())
        
        _ = backbutton.reactive.tap.observeNext {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let button_1 = UIBarButtonItem(customView: backbutton)
        self.navigationItem.leftBarButtonItems = [button_1]
        self.navigationItem.hidesBackButton = true
        
        bookmarkButton.imageView?.frame = CGRect(x: 0,y: 0,width: 35,height: 35)
        bookmarkButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        bookmarkButton.imageView!.contentMode = .scaleAspectFit
        let nonBookmarkedImg = UIImage(named: ImageName.DisabledBookmarkIcon.description)
        bookmarkButton.setImage(nonBookmarkedImg, for: UIControlState())
        bookmarkButton.addTarget(self, action: #selector(PageViewController.bookmarkTapped), for: UIControlEvents.touchUpInside)
        
        let bookmark = UIBarButtonItem(customView: bookmarkButton)
        
        
        shareButton.imageView?.frame = CGRect(x: 0,y: 0,width: 35,height: 35)
        shareButton.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 2, 5)
        shareButton.imageView!.contentMode = .scaleAspectFit
        shareButton.setImage(UIImage(named: ImageName.ShareIcon.description), for: UIControlState())
        shareButton.addTarget(self, action: #selector(PageViewController.shareTapped), for: UIControlEvents.touchUpInside)
        let share = UIBarButtonItem(customView: shareButton)
        
        let negativeButton = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeButton.width = -15
        self.navigationItem.rightBarButtonItems = [negativeButton,bookmark,share]
        
    }
    
    func bookmarkTapped(){
        
        let event = GAIDictionaryBuilder.createEvent(
            withCategory: "page_view_event",
            action: "tapped",
            label: "bookmark",
            value: nil).build()
        tracker?.send(event as! [AnyHashable: Any])
    }
    
    func shareTapped(){
        
        let event = GAIDictionaryBuilder.createEvent(
            withCategory: "page_view_event",
            action: "tapped",
            label: "share",
            value: nil).build()
        tracker?.send(event as! [AnyHashable: Any])
    }
    
    internal func buttonHandlers(_ vm: ArticleViewModel){
        
        //Attach observer to buttons
        //These observers need to be disposed later
        
    
        shareObserver = shareButton.reactive.tap.observeNext {
            let rootVc = UINavigationController.rootNavigationController
            let accVc = UIActivityViewController(activityItems: [self.url as Any], applicationActivities: nil)
            rootVc.present(accVc, animated: true, completion: nil)
            
        } as! SimpleDisposable
        
        
        bookmarkObserver = bookmarkButton.reactive.tap.observeNext {
            
            if (self.bookmarkButton.currentImage == self.nonBookmarkedImg){
                self.bookmarkButton.setImage(self.bookmarkedImg, for: .normal)
                AlertViewManager.sharedInstance.showSuccess(.bookmarked)
                self.realmManager.update{
                    if let news = vm.realmNews {
                        news.homeObject!.isBookmark = true
                    }
                }
            }else{
                self.bookmarkButton.setImage(self.nonBookmarkedImg, for: .normal)
                self.realmManager.update{
                    if let news = vm.realmNews {
                        news.homeObject!.isBookmark = false
                    }
                }
            }
        } as! SimpleDisposable
    }
    
    func observeChangeInValue(_ vm: ArticleViewModel){
        //Dispose observers
     
        if let observerForBookmark = bookmarkObserver{
            if !observerForBookmark.isDisposed {
                observerForBookmark.dispose()
            }
        }
        
        if let observerForShare = shareObserver{
            if !observerForShare.isDisposed{
                observerForShare.dispose()
            }
        }
        
        //Attach observer
        _ = vm.isBookmarked.observeNext { (value) in
            if value {
                self.bookmarkButton.setImage(self.bookmarkedImg, for: .normal)
            }
            else {
                self.bookmarkButton.setImage(self.nonBookmarkedImg, for: .normal)
            }
        }
        
        _ = vm.url.observeNext { (value) in
            self.url = URL(string: value)!
        }
        
        buttonHandlers(vm)
    }
}
