//
//  PageViewControllerExtension.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/24/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//

import UIKit


//MARK: -Delegate and datasource


extension PageViewController : UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        
        guard let vcIndex = viewModel.vcArray.index(of: viewController as! ArticleViewController) else { return nil }
        
        
        let prevIndex = vcIndex - 1
        let vcArrayCount = viewModel.vcArray.count
        var vc : ArticleViewController
        guard prevIndex >= 0 else {
            if vcArrayCount>1 {
                vc = viewModel.vcArray.last!
                return vc
            } else {return nil}
        }
        
        guard vcArrayCount > prevIndex else { return nil }
        vc = viewModel.vcArray[prevIndex]
        //vc.bindViewModel()
        return vc
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let vcIndex = viewModel.vcArray.index(of: viewController as! ArticleViewController) else { return nil }
        
        let nextIndex = vcIndex + 1
        let vcArrayCount = viewModel.vcArray.count
        var vc : ArticleViewController
        guard vcArrayCount != nextIndex else {
            if vcArrayCount>1 {
                vc = viewModel.vcArray.first!
                vc.bindViewModel()
                return vc
            } else {return nil}
        }
        
        guard vcArrayCount > nextIndex else { return nil }
        vc = viewModel.vcArray[nextIndex]
        //vc.bindViewModel()
        return vc
    }
    
   
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            let vc = pageViewController.viewControllers?.first as! ArticleViewController
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            observeChangeInValue(vc.viewModel)
            pageControl.currentPage = viewModel.vcArray.index(of: vc)!
        }
    }
    
    
    
}

//Different handlers or reuse function ?
extension PageViewController: PageViewAdapter{
    
    func successHandler(_ ids: [Int]) {
        viewModel.createVcArray()
        setupPageControl(viewModel.vcArray.count)
        if let firstVc = viewModel.vcArray.first {
            let router = ArticleViewRouter(vc: firstVc)
            firstVc.router = router
            setViewControllers([firstVc], direction: .forward, animated: true, completion: nil)
            firstVc.bindViewModel()
            observeChangeInValue(firstVc.viewModel)
        }
        
        
       // AlertViewManager.sharedInstance.stopLoading()

    }
    
    func failureHandler(_ err: Error) {
        viewModel.createVcArray()
        setupPageControl(viewModel.vcArray.count)
        if let firstVc = viewModel.vcArray.first {
            let router = ArticleViewRouter(vc: firstVc)
            firstVc.router = router
            setViewControllers([firstVc], direction: .forward, animated: true, completion: nil)
            observeChangeInValue(firstVc.viewModel)
        }
        AlertViewManager.sharedInstance.stopLoading()
        
    }
}
