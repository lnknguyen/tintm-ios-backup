//
//  RouterComponent.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

//App routing will be handled here
//Router must hold weak reference to view controller
//All customizations for navigation bar should be implemented here
//Custom transitions should also be handled here

import UIKit
import Bond
import Google
//MARK: - Root and side menu
/* Router impl for root view router protocol*/
struct RootViewRouter: RootViewRouterProtocol{
    //Avoid circular dependency -> using weak here
    weak var sideMenuViewController : SideMenuViewController?
    
    init(vc: SideMenuViewController){
        sideMenuViewController = vc
    }
    
    /**
     Push to search view
     */
    func pushFromSideMenuToSearchView() {
        let tracker = GAI.sharedInstance().defaultTracker
        let event = GAIDictionaryBuilder.createEvent(
            withCategory: "sidemenu_event",
            action: "tapped",
            label: "goto_search",
            value: nil).build()
        tracker?.send(event as! [AnyHashable: Any])
        
        let vc = SearchViewController(style: .plain)
        let viewModel = SearchViewModel()
        let router = SearchViewRouter(vc: vc)
        vc.router = router
        vc.viewModel = viewModel
        let navVc = sideMenuViewController?.navigationController as! UISideMenuNavigationController
        navVc.dismiss(animated: true, completion: nil)
        let rootVc = UINavigationController.rootNavigationController
        rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action:nil )
        vc.title = "Tìm kiếm"
        rootVc.pushViewController(vc, animated: true)
    }
    
    /**
     Push to bookmark view
     */
    func pushFromSideMenuToBookmarkView() {
        let tracker = GAI.sharedInstance().defaultTracker
        let event = GAIDictionaryBuilder.createEvent(
            withCategory: "sidemenu_event",
            action: "tapped",
            label: "goto_bookmark",
            value: nil).build()
        tracker?.send(event as! [AnyHashable: Any])
        
        let vc = BookmarkViewController(style: .plain)
        let viewModel = BookmarkViewModel()
        let router = BookmarkViewRouter(vc: vc, vm: viewModel)
        vc.router = router
        vc.viewModel = viewModel
        let navVc = sideMenuViewController?.navigationController as! UISideMenuNavigationController
        navVc.dismiss(animated: true, completion: nil)
        let rootVc = UINavigationController.rootNavigationController
        rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action:nil )
        vc.title = "Tin đã lưu"
        rootVc.pushViewController(vc, animated: true)
        
    }
    
    /**
     Push to setting view
     */
    func pushFromSideMenuToSettingsView() {
        let tracker = GAI.sharedInstance().defaultTracker
        let event = GAIDictionaryBuilder.createEvent(
            withCategory: "sidemenu_event",
            action: "tapped",
            label: "goto_setting",
            value: nil).build()
        tracker?.send(event as! [AnyHashable: Any])
        
        let vc = SettingViewController()
        let viewModel = SettingViewModel()
        let router = SettingViewRouter(vc: vc, vm: viewModel)
        vc.viewModel = viewModel
        vc.router = router
        let navVc = sideMenuViewController?.navigationController as! UISideMenuNavigationController
        navVc.dismiss(animated: true, completion: nil)
        let rootVc = UINavigationController.rootNavigationController
        rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: vc, action:nil )
        vc.title = "Cài đặt"
        rootVc.pushViewController(vc, animated: true)
    }
}

//MARK: - Home
/* Router impl for home view */
struct HomeViewRouter : HomeViewRouterProtocol {
    weak var homeViewController : HomeViewController?
    var homeViewModel : HomeViewModel?
    
    init(vc: HomeViewController, vm: HomeViewModel){
        homeViewController = vc
        homeViewModel = vm
        
    }
    
    /**
     Push to an article view
     
     - parameter row: row in which the cell is selected
     */
    func pushFromHomeViewToArticle(_ row: Int) {
        let vm = homeViewController?.viewModel
        if let homeViewModel = vm{
            var id : Int
            if appDataObject!.isOffline == false {
                id = homeViewModel.observableIds.value[row]
            }else {
                id = homeViewModel.offlineDataSource[row].id
            }
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            let categoryName = homeViewModel.name
            
            //setup navigation
            let rootVc = UINavigationController.rootNavigationController
            rootVc.isNavigationBarHidden = false
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: categoryName, style: .plain, target: nil, action: nil)
            rootVc.pushViewController(vc, animated: false)
        }else{
            fatalError("View model must not be nil")
        }
    }
    
    /**
     Push to a list of feeds
     
     - parameter row: row in which cell is selected
     */
    func pushFromHomeViewToPageView(_ row: Int) {
        let vm = homeViewController?.viewModel
        if let homeViewModel = vm{
            var id : Int
            
            if appDataObject!.isOffline == false {
                id = homeViewModel.observableFeeds[row].id
            }else {
                id = homeViewModel.offlineDataSource[row].id
            }
            
            var viewModel = PageViewViewModel(id: id)
            
            if appDataObject!.isOffline == false {
                var tempIds = Array(homeViewModel.observableFeeds.map { en in return en.id})
                tempIds += tempIds[0...7]
                viewModel.idList = Array(tempIds[row+1...row+6])
            }else {
                var tempIds = homeViewModel.offlineDataSource
                tempIds += homeViewModel.offlineDataSource[0...7]
                viewModel.idList = Array(tempIds[row+1...row+6]).map{ en in
                    return en.id
                }
            }
            
            
            let vc = PageViewController()
            vc.viewModel = viewModel
            
            
            //setup navigation
            let rootVc = UINavigationController.rootNavigationController
            rootVc.isNavigationBarHidden = false
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            rootVc.pushViewController(vc, animated: false)
        }else{
            fatalError("View model must not be nil")
        }
        
    }
    
    /**
     Push to a list of feeds that belongs to a trend
     
     - parameter row: row in which cell is selected
     */
    func pushFromHomeViewToTrendView(_ row: Int){
        guard let entity = homeViewModel?.observableTrends[row] else { return }
        let viewModel = TrendViewModel(id: entity.id)
        let vc = TrendViewController()
        let router = TrendViewRouter(vc: vc, vm: viewModel)
        vc.viewModel = viewModel
        vc.view.backgroundColor = UIColor.white
        vc.router = router
        let navVc = UINavigationController(rootViewController: vc)
        navVc.isNavigationBarHidden = true
        vc.title = entity.title
        let rootVc = UINavigationController.rootNavigationController
        rootVc.isNavigationBarHidden = false
        rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        rootVc.pushViewController(vc, animated: false)
    }
    
}


//MARK: - Article
/* Router impl for article view*/
struct ArticleViewRouter : ArticleViewRouterProtocol{
    weak var articleViewController : ArticleViewController?
    
    init(vc: ArticleViewController){
        articleViewController = vc
    }
    
    func popFromArticleViewToHomeView() {
        /* Impl later */
        let navVc = articleViewController?.navigationController
        navVc?.isNavigationBarHidden = true
        navVc?.popViewController(animated: true)
    }
    
    func popFromArticleViewToBookmarkView() {
        /* Impl later*/
        
        let navVc = articleViewController?.navigationController
        navVc?.isNavigationBarHidden = false
        navVc?.popViewController(animated: true)
        
    }
    
    func pushFromArticleViewToRelatedArticleView(_ row: Int) {
        let vm = articleViewController?.viewModel
        if let articleViewModel = vm{
            let id = articleViewModel.relatedFeeds.value[row].id
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            let navVc = articleViewController!.navigationController
            navVc!.navigationItem.hidesBackButton = true
            
            navVc!.pushViewController(vc, animated: true)
            
        }else{
            fatalError("View model must not be nil")
        }
        
    }
    
    func popToRootView(){
        let navVc = articleViewController!.navigationController
        navVc!.popToRootViewController(animated: true)
    }
}

//MARK: - Bookmark
/* Router impl for bookmark view*/

struct BookmarkViewRouter : BookmarkViewRouterProtocol{
    weak var bookmarkViewController : BookmarkViewController?
    var bookmarkViewModel : BookmarkViewModel?
    
    init(vc: BookmarkViewController, vm: BookmarkViewModel){
        bookmarkViewController = vc
        bookmarkViewModel = vm
    }
    
    func popFromBookmarkViewToHomeView() {
        /* Impl later */
    }
    
    /**
     Push to an article view
     
     - parameter row: row in which the cell is selected
     */
    func pushFromBookmarkViewToArticleView(_ row: Int) {
        if let bookmarkViewModel = bookmarkViewModel{
            let id = bookmarkViewModel.dataArray[row].id
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            
            let rootVc = UINavigationController.rootNavigationController
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "Bookmarks", style: .plain, target: nil, action: nil)
            let bookmark = UIBarButtonItem(title: "", style: .plain, target: vc, action: nil)
            rootVc.navigationItem.rightBarButtonItem = bookmark
            rootVc.pushViewController(vc, animated: true)
        }else{
            fatalError("View model must not be nil")
        }
        
    }
}

//MARK: - Setting

struct SettingViewRouter : SettingRouterProtocol{
    weak var settingViewController : SettingViewController?
    var settingViewModel : SettingViewModel?
    
    init(vc: SettingViewController, vm: SettingViewModel){
        settingViewController = vc
        settingViewModel = vm
    }
    
    
    /**
     Push to website
     */
    func pushToWebsiteView() {
        UIApplication.shared.openURL(URL(string: "http://www.tintm.com/")!)
    }
    
    /**
     Push to mail composer
     */
    func pushToMailView() {
        let vc = MailViewController()
        settingViewController?.present(vc, animated: true, completion: nil)
    }
    
    /**
     Push to info view
     */
    
    func pushToInfoView() {
        let vc = LibraryViewController(fileName: "info")
        vc.title = "Thông tin"
        settingViewController!.navigationController?.navigationBar.topItem!.backBarButtonItem =  UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        settingViewController!.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func pushToNewsSourceConfigurationView() {
        let vc = NewsSourceViewController()
        settingViewController!.navigationController?.navigationBar.topItem!.backBarButtonItem =  UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        settingViewController?.navigationController?.pushViewController(vc, animated: true)
    }
    /**
     Present delete news
     */
    func presentDeleteOfflineNews() {
        let alert = UIAlertController(title: "", message: "Xoá tất cả tin đã lưu ?", preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Đồng ý", style: .default) { (alert: UIAlertAction!) in
            let realmManager = RealmCommandManager()
            realmManager.clear(.home, excludeBookmark: false)
            realmManager.clear(.news, excludeBookmark: false)
            AlertViewManager.sharedInstance.showSuccess(.doneDeletingNews)
        }
        
        let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel) { (alert: UIAlertAction!) in
            
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        
        settingViewController!.present(alert, animated: true, completion: nil)
    }
}

struct SearchViewRouter: SearchViewRouterProtocol{
    weak var searchViewController : SearchViewController?
    
    init(vc: SearchViewController){
        searchViewController = vc
    }
    
    /**
     Push to an article view
     
     - parameter row: row in which the cell is selected
     */
    func pushFromSearchViewToArticleView(_ row: Int) {
        let vm = searchViewController?.viewModel
        if let searchViewModel = vm{
            let id = searchViewModel.filteredRealmObject[row].id
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            let rootVc = UINavigationController.rootNavigationController
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            let bookmark = UIBarButtonItem(title: "", style: .plain, target: vc, action: nil)
            rootVc.navigationItem.rightBarButtonItem = bookmark
            rootVc.pushViewController(vc, animated: true)
        }else{
            fatalError("View model must not be nil")
        }
    }
    
    func popFromSearchViewToHomeView() {
        /*Impl later*/
    }
}


//MARK: - Trend
/* Router impl for trend view */
struct TrendViewRouter : TrendViewRouterProtocol {
    weak var trendViewController : TrendViewController?
    var trendViewModel : TrendViewModel?
    
    init(vc: TrendViewController, vm: TrendViewModel){
        trendViewController = vc
        trendViewModel = vm
        
    }
    
    /**
     Push to an article view
     
     - parameter row: row in which the cell is selected
     */
    func pushFromTrendViewToArticle(_ row: Int) {
        let vm = trendViewController?.viewModel
        if let trendViewModel = vm{
            var id : Int
            
            id = trendViewModel.observableIds.value[row]
            
            let viewModel = ArticleViewModel(id: id)
            let vc = ArticleViewController()
            let router = ArticleViewRouter(vc: vc)
            vc.router = router
            vc.viewModel = viewModel
            let categoryName = trendViewModel.name
            
            //setup navigation
            let rootVc = UINavigationController.rootNavigationController
            rootVc.isNavigationBarHidden = false
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: categoryName, style: .plain, target: nil, action: nil)
            rootVc.pushViewController(vc, animated: false)
        }else{
            fatalError("View model must not be nil")
        }
    }
    
    /**
     Push to a list of feeds
     
     - parameter row: row in which cell is selected
     */
    func pushFromTrendViewToPageView(_ row: Int) {
        let vm = trendViewController?.viewModel
        if let trendViewModel = vm{
            var id : Int
            
            id = trendViewModel.observableFeeds[row].id
            
            var viewModel = PageViewViewModel(id: id)
            
            
            var tempIds = Array(trendViewModel.observableFeeds.map { en in return en.id})
            tempIds += tempIds[0...7]
            viewModel.idList = Array(tempIds[row+1...row+6])
        
            let vc = PageViewController()
            vc.viewModel = viewModel
            
            //setup navigation
            let rootVc = UINavigationController.rootNavigationController
            rootVc.isNavigationBarHidden = false
            rootVc.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            rootVc.pushViewController(vc, animated: false)
        }else{
            fatalError("View model must not be nil")
        }
        
    }
    
}
