//
//  ButtonPresentable.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/28/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

protocol ButtonPresentable {
    var backgroundColor : UIColor { get }
    var textColor : UIColor { get }
    var backgroundImage : UIImage { get }
    var image : UIImage { get }
    var imageInsets : UIEdgeInsets { get }
}

extension ButtonPresentable {
    var backgroundColor : UIColor { return UIColor.clear }
    var textColor : UIColor { return UIColor.white }
    var imageInsets : UIEdgeInsets { return UIEdgeInsetsMake(5, 5, 5, 5)}
}
