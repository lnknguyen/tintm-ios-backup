//
//  TableViewPresentable.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit


protocol TableViewPresentable {
    var showVerticalScrollIndicator : Bool { get }
    var allowsSelection : Bool { get }
    var separatorColor : UIColor { get }
    var rowHeight : CGFloat { get }
}

protocol HomeCellPresentable {
    var titleView : UILabel { get }
    var sourceView: UILabel { get }
    var imgView : UIImageView { get }
    var summaryView : UILabel { get }
    var timestampView :UILabel { get }
}

extension TableViewPresentable {
    var showVerticalScrollIndicator : Bool { return false}
    var allowsSelection : Bool { return true }
    var separatorColor : UIColor { return UIColor.gray }
    //Default option is dynamic height
    var rowHeight : CGFloat { return UITableViewAutomaticDimension }
}

extension HomeCellPresentable {
   
}
