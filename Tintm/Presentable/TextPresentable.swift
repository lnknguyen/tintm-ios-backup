//
//  Tintm.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//



import UIKit

//MARK: Text protocol
protocol TextPresentable {
    //Color 
    var backgroundColor : UIColor { get }
    var textColor : UIColor { get }
    var sourceNameColor : UIColor { get }
    var timeStampColor : UIColor { get }
    
    //Font
    var textFont : UIFont { get }
    var headingFont : UIFont { get }
    var subheadingFont : UIFont { get }
    var summaryFont: UIFont { get }
    var sourceNameFont: UIFont { get }
    var timeStampFont: UIFont { get}
    
    //Stuff
    var margin : UIEdgeInsets { get }
    var selectable : Bool { get }
    var align : NSTextAlignment { get }
    var numberOfLines : Int { get }
    var lineBreakMode : NSLineBreakMode { get }
}



//MARK: - Text color
extension TextPresentable {
    var backgroundColor : UIColor {
        return UIColor.white
    }
    
    var textColor: UIColor {
        return UIColor.black
    }
    
    var sourceNameColor : UIColor {
        return UIColor.appColor()
    }
    
    var timeStampColor : UIColor {
        return UIColor.gray
    }
}

//MARK: - Text font
extension TextPresentable {
    var headingFont : UIFont {
        return UIFont.appBoldFont(15)
    }
    
    var subheadingFont : UIFont {
        return UIFont.appBoldFont(14)
    }
    
    var sourceNameFont: UIFont {
        return UIFont.appBoldFont(10)
    }
    
    var timeStampFont : UIFont {
        return UIFont.appRegularFont(10)
    }

}

//MARK: - Text stuff
extension TextPresentable{
    var margin : UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    var textFont : UIFont {
        return UIFont.appRegularFont(15)
    }
    
    var summaryFont: UIFont {
        return UIFont.appRegularFont(15)
    }
    
    var align : NSTextAlignment {
        return NSTextAlignment.left
    }
    
    var numberOfLines: Int {
        return 0
    }
    
    var lineBreakMode : NSLineBreakMode {
        return NSLineBreakMode.byTruncatingTail
    }
    
    var selectable : Bool {
        return false
    }

}

//MARK: - Image Protocol
protocol ImagePresentable {
    var imageName: String { get }
}

//MARK: Image extension
extension ImagePresentable {
    var imageName: String {
        return "tintm-place-holder"
    }
}
