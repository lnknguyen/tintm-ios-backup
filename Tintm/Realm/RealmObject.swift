//
//  RealmObject.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/21/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import RealmSwift

//Realm object to be displayed as an article
class RealmNewsObject: Object{
    dynamic var content = ""
    dynamic var homeObject: RealmHomeObject?
    dynamic var id = 0
        
    convenience init(homeObject: RealmHomeObject,content: String){
        self.init()
        self.content = content
        self.homeObject = homeObject
        self.id = homeObject.id
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    
}

//Realm object to be displayed in home view
class RealmHomeObject: Object {
    dynamic var id = 0
    dynamic var categoryId = 0
    dynamic var sourceName = ""
    dynamic var summary = ""
    dynamic var timestamp = ""
    dynamic var title = ""
    dynamic var thumbnail = ""
    dynamic var image = ""
    dynamic var related = 0
    dynamic var isBookmark = false
    dynamic var tintmUrl = "http://www.tintm.com"
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id: Int, categoryId: Int, sourceName: String, summary: String, timestamp: String, title: String, thumbnail: String, image: String, related: Int, tintmUrl: String){
        self.init()
        self.id = id
        self.sourceName = sourceName
        self.categoryId = categoryId
        self.summary = summary
        self.timestamp = timestamp
        self.image = image
        self.title = title
        self.thumbnail = thumbnail
        self.related = related
        self.tintmUrl = tintmUrl
    }
    

}

//Realm category object
class RealmCategoryObject: Object {
    dynamic var id : Int = 0
    dynamic var interest: Int = 0
    dynamic var name: String = ""
    dynamic var slug: String = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id: Int, interest: Int, name: String, slug: String) {
        self.init()
        self.id = id
        self.interest = interest
        self.name = name
        self.slug = slug
    }
}

//Realm app data object
class RealmAppDataObject: Object {
    dynamic var id : Int = 0
    dynamic var isOffline: Bool = false
    dynamic var allowOffline : Bool = false
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(id: Int, isOffline: Bool, allowOffline: Bool) {
        self.init()
        self.id = id
        self.isOffline = isOffline
        self.allowOffline = allowOffline
    }
}