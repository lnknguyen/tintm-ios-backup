//
//  RealmCommand.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/24/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import RealmSwift

enum RealmObjectType {
    case home
    case news
    case category
}

protocol Command {
    associatedtype Target
}

/**
 * Command to create new realm instances
 */
struct CreateCommand : Command {
    let realm = try! Realm()
    typealias Target = Entity
    
    func execute(_ entity: [Target]) -> [Object] {
        
        if let res = entity as? [HomeNewsEntity]{
            return res.map({ (e) -> RealmHomeObject in
                return RealmHomeObject(id: e.id, categoryId: e.categoryId, sourceName: e.sourceName, summary: e.summary, timestamp: e.timestamp, title: e.title, thumbnail: e.thumbnail, image: e.image, related: e.related, tintmUrl: e.tintmUrl)
            })
        } else if let res = entity as? [NewsEntity]{
            return res.map({ (e) -> RealmNewsObject in
                let homeObject = RealmHomeObject(id: e.id, categoryId: e.categoryId, sourceName: e.sourceName, summary: e.summary, timestamp: e.timestamp, title: e.title, thumbnail: "", image: "", related: 0, tintmUrl: e.tintmUrl)
                //Convert to html string before passing to realm object
                let htmlString = String.toHtml(e.title, sourceName: e.sourceName, timeStamp: e.timestamp, content: e.content)
                return RealmNewsObject(homeObject: homeObject, content: htmlString)
            })
        } else if let res = entity as? [CategoryEntity]{
            return res.map({ (e) -> RealmCategoryObject in
                return RealmCategoryObject(id: e.id, interest: e.interest, name: e.name, slug: e.slug)
            })
        }
        
        
        return [Object]()
    }
    
    func execute(_ entity: Target) -> Object {
        
        if let e = entity as? HomeNewsEntity{
            return RealmHomeObject(id: e.id, categoryId: e.categoryId, sourceName: e.sourceName, summary: e.summary, timestamp: e.timestamp, title: e.title, thumbnail: e.thumbnail, image: e.image, related: e.related, tintmUrl: e.tintmUrl)
            
        } else if let e = entity as? NewsEntity{
            let homeObject = RealmHomeObject(id: e.id, categoryId: e.categoryId, sourceName: e.sourceName, summary: e.summary, timestamp: e.timestamp, title: e.title, thumbnail: "", image: "", related: 0, tintmUrl: e.tintmUrl)
            //Convert to html string before passing to realm object
            let htmlString = String.toHtml(e.title, sourceName: e.sourceName, timeStamp: e.timestamp, content: e.content)
            return RealmNewsObject(homeObject: homeObject, content: htmlString)
        }else if let e = entity as? CategoryEntity{
            return RealmCategoryObject(id: e.id, interest: e.interest, name: e.name, slug: e.slug)
        }
        
        return Object()
    }
    
}

/**
 *  Command to read from realm database
 */
struct ReadCommand : Command {
    let realm = try! Realm()
    typealias Target = RealmObjectType
    
    
    func execute(_ type: Target,dictionary: [String:AnyObject]) -> [Object] {
        //assert(dictionary.count == 0,"Dictionary cannot be empty")
        if dictionary.count > 0 {
            let key = dictionary.keys.first!
            let value = dictionary[key]!
            
            switch type{
            case .home:
                let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
                return Array(realm.objects(RealmHomeObject.self).filter(predicate))
            case .news:
                let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
                return Array(realm.objects(RealmNewsObject.self).filter(predicate))
            case .category:
                let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
                return Array(realm.objects(RealmCategoryObject.self).filter(predicate))
            }
            
        }else{
            
            switch type{
            case .home:
                return Array(realm.objects(RealmHomeObject.self))
            case .news:
                return Array(realm.objects(RealmNewsObject.self))
            case .category:
                return Array(realm.objects(RealmCategoryObject.self))
            }
        }
    }
    
    func execute(_ type: Target,dictionary: [String:AnyObject]) -> Bool{
        let key = dictionary.keys.first!
        let value = dictionary[key]!
        
        
        switch type{
        case .home:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmHomeObject.self).filter(predicate)).count > 0
        case .news:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmNewsObject.self).filter(predicate)).count > 0
        case .category:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmCategoryObject.self).filter(predicate)).count > 0
        }
    }
    
    func executeSearch(_ type: Target, dictionary: [String:AnyObject])->[Object]{
        let key = dictionary.keys.first!
        let value = NSMutableString(string: dictionary[key] as! String)
        switch type{
        case .home:
            let predicate = NSPredicate(format: "%K CONTAINS[c] %@",argumentArray: [key,value])
            return Array(realm.objects(RealmHomeObject.self).filter(predicate))
        case .news:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmNewsObject.self).filter(predicate))
        case .category:
            let predicate = NSPredicate(format: "%K == %@",argumentArray: [key,value])
            return Array(realm.objects(RealmCategoryObject.self).filter(predicate))
        }
    }
    
}

/**
 *  Command to write to realm database
 */
struct WriteCommand : Command {
    let realm = try! Realm()
    typealias Target = Object
    
    func execute(_ object: Target) {
        try! realm.write({
            realm.add(object, update: true)
        })
    }
    
    func execute(_ objects: [Target]) {
        for e in objects{
            try! realm.write({
                realm.add(e, update: true)
            })
        }
    }
}

/**
 *  Command to delete objects from realm database
 */
struct DeleteCommand : Command {
    let realm = try! Realm()
    typealias Target = RealmObjectType
    
    func execute(_ type: Target, excludeBookmark: Bool){
        
        if !excludeBookmark{
            try! realm.write({
                switch type{
                case .home:
                    let objects = realm.objects(RealmHomeObject.self)
                    realm.delete(objects)
                case .news:
                    let objects = realm.objects(RealmNewsObject.self)
                    realm.delete(objects)
                case .category:
                    let objects = realm.objects(RealmCategoryObject.self)
                    realm.delete(objects)
                }
            })
        } else {
            try! realm.write({
                switch type{
                case .home:
                    let objects = Array(realm.objects(RealmHomeObject.self))
                    for obj in objects {
                        if (!obj.isBookmark){
                            realm.delete(obj)
                        }
                    }
                case .news:
                    let objects = Array(realm.objects(RealmNewsObject.self))
                    for obj in objects {
                        if (!obj.homeObject!.isBookmark){
                            realm.delete(obj)
                        }
                    }
                case .category:
                    let objects = realm.objects(RealmCategoryObject.self)
                    realm.delete(objects)
                }
            })
        }
    }
    
    func execute(_ type: Target, dictionary: [String:AnyObject]){
        try! realm.write({
            let key = dictionary.keys.first!
            let value = dictionary[key]
            let predicate = NSPredicate(format: "%@ = %@",key,value as! String)
            switch type{
            case .home:
                let objects = Array(realm.objects(RealmHomeObject.self).filter(predicate))
                realm.delete(objects)
            case .news:
                let objects = Array(realm.objects(RealmNewsObject.self).filter(predicate))
                realm.delete(objects)
            case .category:
                let objects = Array(realm.objects(RealmCategoryObject.self).filter(predicate))
                realm.delete(objects)
            }
            
        })
    }
    
    func executeClearingByDate(_ type: Target, value: Int){
        try! realm.write({
            switch type {
            case .home:
                let objects = Array(realm.objects(RealmHomeObject.self))
                for obj in objects {
                    if (obj.timestamp.toDate().isGreaterThan(NSDate() as Date, unit: NSCalendar.Unit.day, amount: value)
                        && !obj.isBookmark){
                        realm.delete(obj)
                    }
                }
            case .news:
                let objects = Array(realm.objects(RealmNewsObject.self))
                
                for obj in objects {
                    
                    if let homeObject = obj.homeObject {
                        if (homeObject.timestamp.toDate().isGreaterThan(NSDate() as Date, unit: NSCalendar.Unit.day, amount: value)
                            && !homeObject.isBookmark){
                            realm.delete(obj)
                        }
                    }
                }
            case .category:
                fatalError("Cant  clear by date")
            }
        })
    }
    
    func executeClearingByDate(_ value: Int){
        try! realm.write({
            let homeObjects = Array(realm.objects(RealmHomeObject.self))
            let newsObjects = Array(realm.objects(RealmNewsObject.self))
            for home in homeObjects {
                if (home.timestamp.toDate().isGreaterThan(NSDate() as Date, unit: NSCalendar.Unit.day, amount: value)
                    && !home.isBookmark){
                    realm.delete(home)
                    for news in newsObjects {
                        if news.id == home.id { realm.delete(news) }
                    }
                }
            }
        })
    }
}

/**
 *  Command to update an element
 */
struct UpdateCommand : Command {
    let realm = try! Realm()
    typealias Target = Void
    
    func execute(_ closure: @escaping (Target) -> Void){
        /*realm.beginWrite()
         closure()
         try! realm.commitWrite()*/
        do {
            try realm.write{
                closure()
            }
        } catch {
            print("Fatal error update")
        }
    }
}

/**
 *  Command to perform migration
 */

struct MigrateCommand : Command {
    typealias Target = Object
    
    //TODO: Quick implementation. Improve to generic execution later
    func execute(){
        _  = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 1 {
                    migration.enumerateObjects(ofType: RealmAppDataObject.className(), { (oldObject, newObject) in
                        newObject!["allowOffline"] = false
                    })
                }
        })
        
    }
}
