//
//  NetworkManager.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/1/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//
import PromiseKit
import Bond
class NetworkManager {
    static let sharedInstance = NetworkManager()
    
    
    fileprivate init() {}
    /**
     Query categories
     
     - parameter completionHandler: handler
     */
    func getCategories(_ completionHandler:@escaping (ResultEntity<[CategoryEntity]>)->Void){
        WebService.sharedInsance.queryAllCategories()
            .then { (res) -> Void in
                let en = ResultEntity<[CategoryEntity]>(res: res)
                completionHandler(en)
            }.catch { (err) in
                let en = ResultEntity<[CategoryEntity]>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query feeds id by their category id
     
     - parameter id:                category id
     - parameter completionHandler: handler
     */
    
    func getFeedsId(_ id: Int, completionHandler: @escaping (ResultEntity<[Int]>) -> Void) {
        WebService.sharedInsance.queryHighlightNewsIdByCategoryId(id)
            .then { (res) -> Void in
                let en = ResultEntity<[Int]>(res:res)
                completionHandler(en)
            }.catch { (err) in
                let en = ResultEntity<[Int]>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query feed with detail omitted, pass it to a handler
     
     - parameter id:                feed's id
     - parameter completionHandler: handler
     */
    func getFeeds(_ id: Int, completionHandler: @escaping (ResultEntity<HomeNewsEntity>) -> Void) {
        WebService.sharedInsance.queryForSingleFeed(id)
            .then { (res) -> Void in
                let en = ResultEntity<HomeNewsEntity>(res: res)
                completionHandler(en)
            }.catch { (err) in
                let en = ResultEntity<HomeNewsEntity>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query feed detail
     
     - parameter id:                feed's id
     - parameter completionHandler: handle feed detail
     */
    func getFeedDetail(_ id: Int, completionHandler: @escaping (ResultEntity<NewsEntity>) -> Void) {
        WebService.sharedInsance.queryNewsDetailById(id)
            .then { (res) -> Void in
                let en = ResultEntity<NewsEntity>(res: res)
                completionHandler(en)
            }.catch { (err) in
                let en = ResultEntity<NewsEntity>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query for more feeds, maximum is 10
     Store feed in db, executed in background thread
     
     - parameter ids:               array of ids
     - parameter offset:            index to begin
     - parameter completionHandler: handle returned array
     */
    
    func getMoreFeeds(_ ids: [Int], offset: Int, count: Int = 9, completionHandler: @escaping (ResultEntity<[HomeNewsEntity]>) -> Void){
        WebService.sharedInsance.queryForMoreFeedsFromCategory(ids, offset: offset, count: count)
            .then { (res) -> [HomeNewsEntity]? in
                let en = ResultEntity<[HomeNewsEntity]>(res: res)
                completionHandler(en)
                guard let result = en.res else { return nil }
                return result
            }.catch { (err) in
                let en = ResultEntity<[HomeNewsEntity]>(error: err)
                completionHandler(en)
        }
        
    }
    
    /**
     Download and store news content in realm db, executed in background thread
     
     - parameter id: news id
     */
    func storeFeedInBackground(_ id: Int) {
        let backgroundQueue = DispatchQueue.global(qos: .background)
        WebService.sharedInsance.queryNewsDetailById(id)
            .then(on: backgroundQueue) { (res) -> Void in
                let realmManager = RealmCommandManager()
                let realmNewsObj = realmManager.create(res)
                realmManager.write(realmNewsObj)
            }.catch { (err) in
                print("Error storing feed in background")
        }
    }
    
    /**
     Query for related feeds of a feed
     
     - parameter id:                feed's id
     - parameter limit:             number of returned related feeds
     - parameter lastId:            use for paging, optional
     - parameter completionHandler: handlers
     */
    func getRelatedFeeds(_ id: Int, limit: Int = 3, lastId: Int = 0, completionHandler:@escaping (ResultEntity<[HomeNewsEntity]>) -> Void){
        WebService.sharedInsance.queryForRelatedFeeds(id,limit: limit, lastId: lastId)
            .then { (entities) -> Void in
                let en = ResultEntity<[HomeNewsEntity]>(res: entities)
                completionHandler(en)
            }.catch { (err) in
                print(err)
                let en = ResultEntity<[HomeNewsEntity]>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query for current trending search
     
     - parameter completionHandler: handlers
     */
    func getCurrentTrends(_ completionHandler:@escaping (ResultEntity<[TrendsEntity]>) -> Void){
        WebService.sharedInsance.queryForTrends()
            .then{(entities) -> Void in
                let en = ResultEntity<[TrendsEntity]>(res: entities)
                completionHandler(en)
            }.catch{ (err) in
                print(err)
                let en = ResultEntity<[TrendsEntity]>(error: err)
                completionHandler(en)
        }
    }
    
    /**
     Query trend feeds id by their trend's id
     
     - parameter id: Trend's id
     - parameter completionHandler: handler
     */
    
    func getTrendFeedsId(_ id: Int, completionHandler: @escaping (ResultEntity<[Int]>) -> Void) {
        WebService.sharedInsance.queryForTrendNewsIdsByTrendId(id)
            .then { (res) -> Void in
                let en = ResultEntity<[Int]>(res:res)
                completionHandler(en)
            }.catch { (err) in
                let en = ResultEntity<[Int]>(error: err)
                completionHandler(en)
        }
    }
}
