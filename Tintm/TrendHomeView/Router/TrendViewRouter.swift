//
//  TrendViewRouter.swift
//  Tintm
//
//  Created by Nguyen Luong on 10/17/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//

import Foundation

protocol TrendViewRouterProtocol {
    func pushFromTrendViewToArticle(_ row: Int)
    func pushFromTrendViewToPageView(_ row: Int)
}

