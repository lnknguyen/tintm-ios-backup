//
//  TrendViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 10/17/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//

import Foundation


import PromiseKit
import Bond
import RealmSwift


protocol TrendAdapter: class {
    /* Handler for id refreshing success*/
    func successRefreshIds(_ res: [Int], first: Bool )
    /* Handler for successfully load more feeds*/
    func successLoadMore(_ res: [HomeNewsEntity])
    func successHandler(_ res: HomeNewsEntity, cell: BaseHomeCell)
    /* Handler for refresh realm database*/
    func successRefreshRealm(_ res: RealmHomeObject, cell: BaseHomeCell)
    /* Handler for success pulll to refresh*/
    func successPullToRefresh(_ count: Int)
    /* Handler for loading trend*/
    func successLoadTrends(_ res: [TrendsEntity])
    
    /* Handler for exception failure*/
    func failureHandler(_ err: Error)
    func failureLoadMore(_ err: Error)
}

class TrendViewModel:TextPresentable, ImagePresentable{
    
    var id: Int //trend's id
    var name = "" // trend's name
    let realmManager = RealmCommandManager()
    var observableFeeds : MutableObservableArray<HomeNewsEntity>
    var observableIds : Observable<[Int]>{
        didSet{
            print("old:",oldValue)
        }
    }
    //Trend entities array
    var observableTrends: ObservableArray<TrendsEntity>
    
    weak var delegate : TrendAdapter?

    init(id: Int = 0){
        self.id = id
        observableFeeds = MutableObservableArray()
        observableIds = Observable([])
        observableTrends = MutableObservableArray()
    }
    
    convenience init(id: Int, name: String){
        self.init(id: id)
        self.name = name
    }
    
    func preheateFeedsId(){
        NetworkManager.sharedInstance.getTrendFeedsId(id) { (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    self.observableIds.next(value)
                
                }
            case .failure:
                print("Failure preheating")
            }
            
        }
    }
    
    /**
     Refresh list of news id for respectable trend's id 
     */
    func refreshFeedsId(_ first: Bool = false){
        
        
        NetworkManager.sharedInstance.getTrendFeedsId(id) { (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    self.observableIds.next(value)
                    
                    //self.observableIds = value
                    self.delegate?.successRefreshIds(value,first: first)
                }
                else {
                    self.delegate?.failureHandler(ErrorCode.unknownError)
                }
            case .failure:
                self.delegate?.failureHandler(res.error!)
            }
            
        }
    }
    
    
    //Pull to refresh handler
    //Append new ids on top of old ids
    func refresh(){
        
        NetworkManager.sharedInstance.getTrendFeedsId(id) { (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    let insert = value.filter({ (val) -> Bool in
                        !self.observableIds.value.contains(val)
                    })
                    self.observableIds.next(value)
                    guard insert.count > 0 else { return }
                    self.reloadAfterRefresh(insert)
                }
                else {
                    self.delegate?.failureHandler(ErrorCode.unknownError)
                }
            case .failure:
                self.delegate?.failureHandler(res.error!)
            }
        }
    }
    
    // Reload whole table view after pull to refresh
    func reloadAfterRefresh(_ values: [Int]){
        
        NetworkManager.sharedInstance.getMoreFeeds(observableIds.value, offset: values.count, count: values.count) { (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    self.observableFeeds.insert(contentsOf: value, at: 0)
                    guard values.count > 0 else { return }
                    self.delegate?.successPullToRefresh(values.count)
                }
            case .failure:
                if let err = res.error{
                    self.delegate?.failureLoadMore(err)
                }
            }
        }
    }
    
    /**
     Load 10 more feeds
     
     - parameter offset: index to begin from
     */
    func loadMore(_ offset: Int){
        NetworkManager.sharedInstance.getMoreFeeds(observableIds.value, offset: offset) { (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    self.observableFeeds.insert(contentsOf: value, at: self.observableFeeds.endIndex)
                    self.delegate?.successLoadMore(value)
                }
            case .failure:
                if let err = res.error{
                    self.delegate?.failureLoadMore(err)
                }
            }
        }
    }
    
}
