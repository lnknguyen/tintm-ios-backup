//
//  TrendViewExtension.swift
//  Tintm
//
//  Created by Nguyen Luong on 10/17/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//



import UIKit
import RealmSwift
import Google
extension TrendViewController: UITableViewDataSource, ButtonDelegate{
    //MARK: - Table Data source
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel!.observableFeeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let inpRow = indexPath.row
        
        var returnCell = BaseHomeCell()
        if (inpRow % 7 == 0){
            let cell = (tableView.dequeueReusableCell(withIdentifier: HOME_CELL_TYPE_TWO_ID, for: indexPath) as? HomeCellTypeTwo)!
            cell.tableView = tableView
            returnCell = cell
        }else{
            let cell = (tableView.dequeueReusableCell(withIdentifier: HOME_CELL_TYPE_ONE_ID, for: indexPath) as? HomeCellTypeOne)!
            returnCell  = cell
        }
        let entity = viewModel?.observableFeeds[inpRow]
        //returnCell.viewModel = trendViewModel!
        returnCell.delegate = self
        if let _entity = entity{
            returnCell.publishData(_entity)
            //returnCell.constrainSubview()
        }
        //viewModel?.getFeed(id!, cell: returnCell)
        returnCell.tag = inpRow
        return returnCell
        
        
    }
    
    //Delegate method for button
    func didTapButtonInsideCell(_ sender: UITableViewCell) {
        let cell = sender as! BaseHomeCell
        tableView.beginUpdates()
        switch cell.state{
        case .expanded:
            cell.collapseView()
        case .collapsed:
            cell.expandView()
            
        }
        tableView.layoutIfNeeded()
        tableView.endUpdates()
    }
    
}

extension TrendViewController: UITableViewDelegate{
    //MARK: - Table Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //router?.pushFromHomeViewToArticle(indexPath.row)
        router?.pushFromTrendViewToPageView(indexPath.row)
        
        let tracker = GAI.sharedInstance().defaultTracker
        let event = GAIDictionaryBuilder.createEvent(
            withCategory: "trend_home_event",
            action: "tapped",
            label: (viewModel?.name)! + " cell",
            value: nil).build()
        tracker?.send(event as! [AnyHashable: Any])
    }
    
    //Scroll to top button pops up when user scroll pass 1.5 * screen's height
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let threshold = APP_HEIGHT*1.5
        if (scrollView.contentOffset.y > threshold){
            UIView.animate(withDuration: 1, animations: {
                self.scrollToTopButton.center = self.onScreenCenter
            })
        }else {
            UIView.animate(withDuration: 1, animations: {
                self.scrollToTopButton.center = self.offScreenCenter
            })
        }
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
            
            
            if (!isLoading){
                isLoading = true
                footerActivityIndicator.startAnimating()
                viewModel?.loadMore(offset)
                
            }
            
            
        }else {
            // footerActivityIndicator.stopAnimating()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if ( section == 0){
            let footerView = UIView(frame: CGRect(x: 0,y: 0,width: APP_WIDTH,height: 150))
            footerView.addSubview(footerActivityIndicator)
            footerActivityIndicator.hidesWhenStopped = true
            
            footerActivityIndicator.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint(
                item: footerActivityIndicator,
                attribute: .centerX,
                relatedBy: .equal,
                toItem: footerView,
                attribute: .centerX,
                multiplier: 1.0,
                constant: 0.0
                ).isActive = true
            
            NSLayoutConstraint(
                item: footerActivityIndicator,
                attribute: .centerY,
                relatedBy: .equal,
                toItem: footerView,
                attribute: .centerY,
                multiplier: 1.0,
                constant: 0.0
                ).isActive = true
            
            return footerView
        } else { return nil }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if ((viewModel?.observableFeeds.count)! >= (viewModel?.observableIds.value.count)!){
            return 0.0
        }
        
        return 50.0
    }
}

extension TrendViewController: TrendAdapter{
    //Successfully referesh list of news id
    func successRefreshIds(_ res: [Int], first: Bool) {
        tableView.separatorColor = UIColor.lightGray
//        let realm = try! Realm()
//        try! realm.write({
//            appDataObject!.isOffline = false
//        })
        
        if first{
            viewModel?.loadMore(0)
        } else {
            refreshControl.endRefreshing()
            AlertViewManager.sharedInstance.stopLoading()
        }
        tableView.reloadData()
        
    }
    
    
    
    //Successfully load more feeds
    func successLoadMore(_ res: [HomeNewsEntity])
    {
        tableView.reloadData()
        AlertViewManager.sharedInstance.stopLoading()
        offset += 10
        isLoading = false
        footerActivityIndicator.stopAnimating()
        
        
    }
    
    //Successfully load a single feed
    func successLoadFeed(_ res: HomeNewsEntity){
        let realmNews = viewModel?.realmManager.create(res)
        if let news = realmNews{
            viewModel?.realmManager.write(news)
            refreshControl.endRefreshing()
        }
        
    }
    
    //Successfully load entity into cell
    func successHandler(_ res: HomeNewsEntity, cell: BaseHomeCell){
        refreshControl.endRefreshing()
        AlertViewManager.sharedInstance.stopLoading()
    }
    
    //Successfully load realm data into cell
    func successRefreshRealm(_ res: RealmHomeObject, cell: BaseHomeCell){
        cell.publishData(res)
        cell.constrainSubview()
        refreshControl.endRefreshing()
        AlertViewManager.sharedInstance.stopLoading()
    }
    
    func successPullToRefresh(_ count: Int){
        tableView.beginUpdates()
        var pathArray = [IndexPath]()
        for index in 0...count-1{
            pathArray.append(IndexPath(row: index, section: 0))
        }
        tableView.insertRows(at: pathArray, with: .middle)
        tableView.endUpdates()
    }
    
    func successLoadTrends(_ res: [TrendsEntity]) {
        tableView.reloadData()
    }
    //Failure with error
    func failureHandler(_ err: Error) {
        
        print("Error in vc:",err)
        tableView.separatorColor = UIColor.lightGray
        refreshControl.endRefreshing()
        /*let emptyView = StateView(frame: CGRectMake(0, 0, APP_WIDTH, APP_HEIGHT), state: .Empty)
         tableView.backgroundView = emptyView
         tableView.separatorColor = UIColor.clearColor()*/
        AlertViewManager.sharedInstance.stopLoading()
    }
    
    func failureLoadMore(_ err: Error){
        print("Error load more :",err)
        footerActivityIndicator.stopAnimating()
        tableView.tableFooterView = nil
        tableView.contentInset = UIEdgeInsetsMake(0, 0, -50, 0)
    }
}




