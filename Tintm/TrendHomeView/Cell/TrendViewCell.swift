//
//  TrendViewCell.swift
//  Tintm
//
//  Created by Nguyen Luong on 10/17/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//

import UIKit
import Cartography
import Kingfisher

@IBDesignable
class TrendCell: BaseHomeCell{
    var ratio : CGFloat = 0.65
    weak var tableView : UITableView?
    var didLayout  = false
    typealias TrendCellPresentable = TextPresentable & ImagePresentable
    var trendViewModel = TrendViewModel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: TREND_CELL_ID)
        self.addSubview(expandButton)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    //initial constraint
    //Param type: 0 if home view, 1 if trend view
    override func constrainSubview(_ type: Int){
        //capture constraint and store into group
        
        constrain(imgView, titleView, sourceView, timestampView,expandButton, replace: group) { (view1, view2, view3, view4, view5) in
            guard let superview = view1.superview else { return }
            view1.top == superview.top
            view1.left == superview.left
            view1.right == superview.right
            (view1.height == view1.width * ratio) ~ 800
            
            view2.top == view1.bottom + 5
            view2.left == view1.left + 10
            view2.right == view1.right - 10
            
            view3.top == view2.bottom + 5
            view3.left == view2.left
            
            view4.top == view3.top
            view4.left == view3.right + 5
            
            view5.right == superview.right
            view5.left == superview.left
            view5.height == 40

            
            (view3.bottom <= superview.bottom - 5) ~ LayoutPriority(100)
            (view4.bottom <= superview.bottom - 5) ~ LayoutPriority(100)
            (view5.bottom <= superview.bottom - 5) ~ LayoutPriority(100)
        }
    }
    
    
    override func publishData(_ feed: HomeNewsEntity) {
        super.publishData(feed)
        self.constrainSubview(0)
        imgView.image = UIImage(named: ImageName.AppIcon.description)
        //imgView.kf_showIndicatorWhenLoading = true
        imgView.contentMode = .scaleToFill
        if let url = URL(string: feed.image) {
            imgView.kf.setImage(with: url, placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: { (received, total) in
                
            }, completionHandler: { (image, error, cacheType, imageURL) in
                if error == nil {
                    
                    guard let _ = image else {
                        // self.imgView.image = UIImage(named: ImageName.AppIcon.description)
                        return
                    }
                }
                else {
                    print("Error setting image")
                    self.imgView.image = UIImage(named: ImageName.AppIcon.description)
                }

            })
        }
    }
    
    override func publishData(_ feed: RealmHomeObject) {
        super.publishData(feed)
    }
    
    
    override func configureSubview(_ presenter: TrendCellPresentable) {
        super.configureSubview(presenter)
    }
    
    
    //Expand and collapse cell
    override func expandView() {
        super.expandView()
        self.addSubview(summaryView)
        summaryView.alpha = 0
        constrain(imgView, summaryView,sourceView, expandButton) { (view1, view2, view3,view4) in
            guard let superview = view1.superview else { return }
            //view2.top == (view1.bottom + 10) ~ 400
            view2.top == (view3.bottom + 15) ~ LayoutPriority(400)
            view2.left == superview.left + 5
            view2.right == superview.right - 5
            view2.bottom <= (superview.bottom - 5) ~ LayoutPriority(600)
            
            view4.top == view2.bottom + 5
            view4.bottom <= (superview.bottom - 5) ~ LayoutPriority(800)
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.summaryView.alpha = 1
        }) 
    }
    
    override func collapseView() {
        super.collapseView()
        summaryView.removeFromSuperview()
        constrain(imgView){view1 in
            view1.bottom <= (view1.superview?.bottom)! - 5
        }
    }
}
