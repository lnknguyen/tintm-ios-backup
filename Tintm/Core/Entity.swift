//
//  Entity.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import ObjectMapper
import Bond
import Realm

protocol Entity: Mappable{
    var id: Int { get }
}


//MARK: - Category entity

struct CategoryEntity: Entity {
    var id : Int = 0
    var interest: Int = 0
    var name: String = ""
    var slug: String = ""
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["Id"]
        interest <- map["Interest"]
        name <- map["Name"]
        slug <- map["Slug"]
    }
}

//MARK: - News detail entity

struct NewsEntity:Entity, Equatable{
    var id: Int = 0
    var categoryId: Int = 0
    var categoryName = ""
    var content = ""
    var sourceName = ""
    var sourceUrl = ""
    var summary = ""
    var timestamp = ""
    var title = ""
    var tintmUrl = ""
    init(){
        
    }
    //Mappable
    init?(map: Map) {
       
    }
    
    mutating func mapping(map: Map) {
        id <- map["Id"]
        categoryId <- map["CategoryId"]
        categoryName <- map["CategoryName"] 
        content <- map["Content"]
        sourceName <- map["SourceName"]
        sourceUrl <- map["SourceUrl"]
        summary <- map["Summary"]
        timestamp <- map["TimeISO"]
        title <- map["Title"]
        tintmUrl <- map["Url"]
    }
}

//MARK: - Home entity

struct HomeNewsEntity: Entity, Equatable {
    var id: Int = 0
    var timeInMili : Double = 0
    var categoryId : Int = 0
    var categoryName = ""
    var sourceName = ""
    var sourceUrl = ""
    var summary = ""
    var timestamp = ""
    var title = ""
    var thumbnail = ""
    var image = ""
    var related : Int = 0
    var tintmUrl = ""
    init(){
    
    }
    
    //Mappable
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["Id"]
        categoryName <- map["CategoryName"]
        categoryId <- map["CategoryId"]
        sourceName <- map["SourceName"]
        sourceUrl <- map["SourceURL"]
        summary <- map["Summary"]
        title <- map["Title"]
        thumbnail <- map["Thumbnail"]
        timestamp <- map["TimeISO"]
        timeInMili <- map["TimeInMili"]
        image <- map["Image"]
        related <- map["Related"]
        tintmUrl <- map["Url"]
    }
}

//MARK: - Trend entity
struct TrendsEntity: Entity{
    var id: Int = 0
    var title = ""
    var slug = ""
    var image = ""
    var description = ""
    
    //Mappable
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["Id"]
        title <- map["Title"]
        image <- map["Image"]
        slug <- map["Slug"]
        description <- map["Description"]
    }
}

//MARK: - Trend entity
struct NewsSourceEntity: Entity{
    var id: Int = 0
    var icon = ""
    var status = true
    var alias = ""
    var type = ""
    init(){
        
    }
    
    //Mappable
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        id <- map["NewsSourceId"]
        icon <- map["Favicon"]
        status <- map["Status"]
        alias <- map["Alias"]
        type  <- map["Type"]
    }
}

//MARK: - Comparison
func == (lhs: HomeNewsEntity, rhs: HomeNewsEntity) -> Bool{
    return lhs.id == rhs.id
}

func == (lhs: NewsEntity, rhs: NewsEntity) -> Bool{
    return lhs.id == rhs.id
}

//MARK: Result type entity
struct ResultEntity<T> {
    var res : T?
    var status : Result<InfoCode,ErrorCode>
    var error : Error?
    
    init(res: T,status: Result<InfoCode,ErrorCode>,error: Error){
        self.res = res
        self.status = status
        self.error = error
    }
    
    init(res: T){
        self.res = res
        status = .success(InfoCode.doneLoadingNews)
        error = nil
    }
    
    init(error: Error){
        self.error = error
        status = .failure(ErrorCode.unknownError)
        res = nil
    }
}



