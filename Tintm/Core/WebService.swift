//
//  WebService.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//


import Alamofire
import PromiseKit
import ObjectMapper

//Singleton web service
//Cache all by default
class WebService{
    let realmManager = RealmCommandManager()
    let manager : Alamofire.SessionManager
    static let sharedInsance = WebService()
    
    fileprivate init() {
        // Create a shared URL cache
        let memoryCapacity = 500 * 1024 * 1024; // 500 MB
        let diskCapacity = 500 * 1024 * 1024; // 500 MB
        let cache = URLCache(memoryCapacity: memoryCapacity, diskCapacity: diskCapacity, diskPath: "shared_cache")
        
        // Create a custom configuration
        let configuration = URLSessionConfiguration.default
        let defaultHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.httpAdditionalHeaders = defaultHeaders
        configuration.requestCachePolicy = .useProtocolCachePolicy // this is the default
        configuration.urlCache = cache
        
        // Create own manager instance that uses custom configuration
        manager = Alamofire.SessionManager(configuration: configuration)
    }
    
    /**
     Return all categories with their properties
     
     - returns: List of category entites
     */
    func queryAllCategories() -> Promise<[CategoryEntity]>{
        let url = APIPath.category.URLPath
        return Promise{ fulfill, reject in
            manager.request(url)
                .validate()
                .responseJSON { (res) in
                    if res.result.error == nil {
                        if let category = Mapper<CategoryEntity>().mapArray(JSONObject: res.result.value as! [AnyObject]){
                            fulfill(category)
                        }
                    }
                    else{
                        reject(res.result.error!)
                    }
                    
            }
        }
        
    }
    
    /**
     Return all highlight news with respectable category
     
     - returns: List of home news entity
     */
    func queryAllHighlightNews() -> Promise<[Int]>{
        let url = APIPath.hotnews.URLPath
        return Promise{ fulfill, reject in
            manager.request(url)
                .validate()
                .responseJSON { (res) in
                    if res.result.error == nil {
                        fulfill(((res.result.value as AnyObject).value(forKey: "1"))! as! [Int])
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
        }
    }
    
    /**
     Return news lists by category id
     
     - parameter id: category's id
     
     - returns: list of news ids ordered by ranking
     */
    func queryHighlightNewsIdByCategoryId(_ id: Int)->Promise<[Int]>{
        let url = APIPath.hotnews.URLPath
        return Promise{ fulfill, reject in
            manager.request(url)
                .validate()
                .responseJSON { (res) in
                    if res.result.error == nil {
                        if let temp = (res.result.value as AnyObject).value(forKey: String(id)) {
                            let idList : [Int] = temp as! [Int]
                            
                            fulfill(idList)
                        }
                        else{
                            reject(NSError(domain: "Fail to retrieve category", code: 400, userInfo: nil))
                        }
                        
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
        }
        
    }
    
    /**
     Return news by Id
     - parameter id: news' id
     
     - returns: news content
     */
    
    func queryNewsDetailById(_ id: Int) -> Promise<NewsEntity>{
        let url = APIPath.news.URLPath
        return Promise{ fulfill, reject in
            manager.request(url+String(id))
                .validate()
                .responseJSON { (res) in
                    if res.result.error == nil{
                        
                        if let hotnews = Mapper<NewsEntity>().map(JSONObject: res.result.value){
                            fulfill(hotnews)
                        }
                        else {
                            reject(NSError(domain: "Fail to get news", code: 400, userInfo: nil))
                        }
                    }
                    else{
                        reject(res.result.error!)
                    }
            }
        }
    }
    
    /**
     Load every 5 feeds in the category
     
     - parameter id:   array of feeds Id
     - parameter offset: offset of feeds
     - parameter count: number of feeds need to be loadded
     - returns: List of 5 news, begin from offset
     */
    
    func queryForMoreFeedsFromCategory(_ ids: [Int], offset: Int, count: Int = 9) -> Promise<[HomeNewsEntity]> {
        let url = APIPath.newsList.URLPath
        var idChain = ""
        
        //Calculate endpoint from offset
        //Considering edge cases
        if (offset < ids.count-1){
            //Chain news id
            
            let endPoint = offset + count <= ids.count-1 ? offset + count : ids.count - 1
            let subIds = ids[offset...endPoint]
            
            let strs = subIds.map { (id) -> String in
                return String(id)
            }
            
            
            
            //Concatenate id, seperated by commas
            for str in strs {
                idChain += str
                idChain += ","
            }
            
            if idChain.characters.count > 0 {
                idChain = idChain.substring(to: idChain.characters.index(before: idChain.endIndex))
            }
            
            return Promise{fulfill, reject in
                manager.request(url + idChain)
                    .validate()
                    .responseJSON{ (res) in
                        if (res.result.error == nil){
                            if (res.result.value as! [AnyObject]).count > 0{
                                if let newsEntity = Mapper<HomeNewsEntity>().mapArray(JSONObject: res.result.value as! [AnyObject]){
                                    
                                    fulfill(newsEntity)
                                }
                                else{
                                    reject(NSError(domain: "Fail loading more home news", code: 400, userInfo: nil))
                                }
                            }
                        } else {
                            reject(res.result.error!)
                        }
                }
            }
        } else {
            //Reject when offset value is bigger than ids array range
            return Promise{fulfill, reject in
                reject(NSError(domain: "Overbound", code: 403, userInfo: nil))
            }
        }
        
    }
    
    /**
     Load a single feed based on its id
     
     - parameter id:   feeds Id
     - returns: a single feeds entity
     */
    
    func queryForSingleFeed(_ id: Int) -> Promise<HomeNewsEntity> {
        let url = APIPath.newsList.URLPath
        return Promise{fulfill, reject in
            manager.request( url + String(id))
                .validate()
                .responseJSON{ (res) in
                    if (res.result.error == nil){
                        if (res.result.value as! [AnyObject]).count > 0{
                            if let newsEntity = Mapper<HomeNewsEntity>().mapArray(JSONObject: res.result.value as! [AnyObject] ){
                                fulfill(newsEntity[0])
                            }
                                
                            else{
                                reject(NSError(domain: "Fail loading home news", code: 400, userInfo: nil))
                            }
                        }
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
        }
    }
    
    /**
     Load a list of related feeds based on a feed's id
     
     - parameter id:     feed's id
     - parameter limit:  number of related feeds need to be queried
     - parameter lastId: for paging
     
     - returns: list of related feeds
     */
    func queryForRelatedFeeds(_ id: Int, limit: Int = 3, lastId : Int = 0) -> Promise<[HomeNewsEntity]>{
        let url = APIPath.related.URLPath
        let params = ["id":id,"limit":limit]
        return Promise{ fulfill, reject in
            manager.request( url, parameters: params)
                .validate()
                .responseJSON{ (res) in
                    if (res.result.error == nil){
                        if (res.result.value as! [AnyObject]).count > 0 {
                            if let entities = Mapper<HomeNewsEntity>().mapArray(JSONObject: res.result.value as! [AnyObject]){
                                fulfill(entities)
                            }
                        }
                        else {
                            reject(NSError(domain: "Fail loading related news", code: 400, userInfo: nil))
                        }
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
            
        }
    }
    
    /**
     Load a list of trending search
     
     - parameter : none
     
     - returns: list of trends entities
     */
    func queryForTrends() -> Promise<[TrendsEntity]>{
        let url = APIPath.trends.URLPath
        
        return Promise{ fulfill, reject in
            manager.request( url)
                .validate()
                .responseJSON{ (res) in
                    if (res.result.error == nil){
                        if (res.result.value as! [AnyObject]).count > 0 {
                            if let entities = Mapper<TrendsEntity>().mapArray(JSONObject: res.result.value as! [AnyObject]){
                                fulfill(entities)
                            }
                        }
                        else {
                            reject(NSError(domain: "Fail loading trends", code: 400, userInfo: nil))
                        }
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
            
        }
    }
    
    /**
     Load a list of ids for a given trend
     - parameter : id-
     
     - returns: list of trends entities
     */
    func queryForTrendNewsIdsByTrendId(_ id: Int)->Promise<[Int]>{
        let url = APIPath.trendsList.URLPath
        let params = ["id":id]
        
        return Promise{ fulfill, reject in
            manager.request( url, parameters: params)
                .validate()
                .responseJSON { (res) in
                    
                    if res.result.error == nil {
                        if let temp = res.result.value{
                            let idList : [Int] = temp as! [Int]
                            
                            fulfill(idList)
                        }
                        else{
                            reject(NSError(domain: "Fail to retrieve ids for trend", code: 400, userInfo: nil))
                        }
                        
                    }
                    else {
                        reject(res.result.error!)
                    }
            }
        }
        
    }
    
}
