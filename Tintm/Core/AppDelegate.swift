//
//  AppDelegate.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import RealmSwift
import Google
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        //Perform migration if needed ( everytime modifying or adding fields in realm db)
        //Migration needs to be performed before initializing RealmCOmmandManager
        //Or else causing runtime error
        
        // Configure tracker from GoogleService-Info.plist.
        var configureError:NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        // Optional: configure GAI options.
        let gai = GAI.sharedInstance()
        gai?.trackUncaughtExceptions = true  // report uncaught exceptions
        gai?.logger.logLevel = GAILogLevel.none // remove before app release
        
        let config  = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 1 {
                    migration.enumerateObjects(ofType: RealmAppDataObject.className(), { (oldObject, newObject) in
                        newObject!["allowOffline"] = false
                    })
                }
        })
        Realm.Configuration.defaultConfiguration = config
        
        //Ensure that appDataObject is created
        if appDataObject == nil{
            let realm = try! Realm()
            try! realm.write({
                let newObj = RealmAppDataObject()
                realm.add(newObj)
                appDataObject = newObj
            })
        }
        
        if let _ = UserDefaults.standard.string(forKey: "firstTimeLoggedIn"){
            
            let rootVc = RootViewController()
            let rootNavVc = UINavigationController(rootViewController: rootVc)
            rootNavVc.isNavigationBarHidden = true
            rootNavVc.navigationBar.isTranslucent = true
            rootNavVc.navigationBar.barStyle = .black
            rootNavVc.customizeNavigationBar()
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = rootNavVc
            window?.backgroundColor = UIColor.white
            window?.makeKeyAndVisible()
            
        } else {
            UserDefaults.standard.set("true", forKey: "firstTimeLoggedIn")
            let rootVc = NewsSourceViewController()
            rootVc.title = "Mời bạn chọn nguồn tin"
            let rootNavVc = UINavigationController(rootViewController: rootVc)
            rootNavVc.isNavigationBarHidden = true
            rootNavVc.navigationBar.isTranslucent = true
            rootNavVc.navigationBar.barStyle = .black
            rootNavVc.customizeNavigationBar()
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = rootNavVc
            window?.backgroundColor = UIColor.white
            window?.makeKeyAndVisible()
        }
    
        //printFonts()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //If user allows offline storing, delete all unbookmarked realm data that are created before 2 days ago
        //If user doesnt allow offline storing, delete all unbookmarked data
    }
    
    func printFonts() {
        let fontFamilyNames = UIFont.familyNames
        for familyName in fontFamilyNames {
            print("------------------------------")
            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName )
            print("Font Names = [\(names)]")
        }
    }
    
    
    
}

