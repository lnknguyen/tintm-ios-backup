//
//  SearchViewControllerExtension.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/7/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

extension SearchViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        viewModel!.filterContentForSearchText(searchController.searchBar.text!)
        tableView.reloadData()
    }
}

extension SearchViewController{
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return viewModel!.filteredRealmObject.count
        }
        return 0
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SEARCH_CELL_ID, for: indexPath) as! SearchCell
        let object  = viewModel!.filteredRealmObject[indexPath.row]
        
        cell.publishData(object)
        cell.constrainSubview()
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.pushFromSearchViewToArticleView(indexPath.row)
    }
    
}
