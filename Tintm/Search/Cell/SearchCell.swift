//
//  SearchCell.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/7/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography

class SearchCell: UITableViewCell{
    
    var viewModel = HomeViewModel()
    
    var titleView = UILabel()
    var summaryView = UILabel()
    var sourceView = UILabel()
    var timestampView = UILabel()
    var group = ConstraintGroup()
    
    typealias Presenter = TextPresentable
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "")
        self.selectionStyle = .none
        
        self.addSubview(titleView)
        self.addSubview(sourceView)
        self.addSubview(timestampView)
        
        configureSubview(viewModel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    func constrainSubview(){
        constrain(titleView, sourceView, timestampView, replace: group) { (view1, view2, view3) in
            guard let superview = view1.superview else { return }
            
            view1.top == superview.top + 5
            view1.right == superview.right - 10
            view1.left == superview.left + 10
            
            view2.top == view1.bottom + 5
            view2.left == view1.left
            view2.height == 15
            
            view3.top == view2.top
            view3.left == view2.right + 5
            view3.bottom == view2.bottom
            
            (view2.bottom <= superview.bottom - 5) ~ LayoutPriority(100)
        }
        
    }
    
    func configureSubview(_ presenter: Presenter){
        //Set subviews background color to white to avoid blending operation in cell
        //See more: https://medium.com/ios-os-x-development/perfect-smooth-scrolling-in-uitableviews-fd609d5275a5#.ymbe3iumc
        
        titleView.font = presenter.headingFont
        summaryView.font = presenter.summaryFont
        sourceView.font = presenter.sourceNameFont
        timestampView.font = presenter.timeStampFont
        
        sourceView.textColor = presenter.sourceNameColor
        timestampView.textColor = presenter.timeStampColor
        
        for subview in self.subviews{
            if subview is UILabel{
                if let subview = subview as? UILabel
                {
                    subview.numberOfLines = presenter.numberOfLines
                    subview.lineBreakMode = presenter.lineBreakMode
                    subview.sizeToFit()
                    subview.backgroundColor = presenter.backgroundColor
                }
            }
        }
        
        summaryView.numberOfLines = presenter.numberOfLines
        summaryView.lineBreakMode = presenter.lineBreakMode
        summaryView.sizeToFit()
        summaryView.backgroundColor = presenter.backgroundColor
        
    }
    
    
    func publishData(_ feed: RealmHomeObject){
        timestampView.text = feed.timestamp.timePassed
        titleView.text = feed.title
        summaryView.text = feed.summary
        sourceView.text = feed.sourceName
    }
    
}
