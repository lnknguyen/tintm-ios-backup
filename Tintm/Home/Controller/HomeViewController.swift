//
//  HomeViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//


import UIKit


import Cartography



//TODO: Temporarily disable pull to refresh due to conflict in logic
class HomeViewController: UIViewController  {
    var viewModel : HomeViewModel?
    
    
    let tableView = UITableView()
    let scrollToTopButton = UIButton(frame: CGRect(x: 0,y: 0,width: 30,height: 30))
    let refreshControl = UIRefreshControl()
    let offScreenCenter = CGPoint(x: APP_WIDTH*4/5, y: APP_HEIGHT+100)
    let onScreenCenter = CGPoint(x: APP_WIDTH*4/5, y: APP_HEIGHT-100)
    let realmManager = RealmCommandManager()
    var router : HomeViewRouter?
    var footerActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    var trendViewModelArray = [TrendViewModel]()
    //Offset to load more data from
    var offset = 10
    var isLoading = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(tableView)
        tableView.addSubview(refreshControl)
        //tableView.addSubview(footerRefresh)
        self.view.addSubview(scrollToTopButton)
        
        AlertViewManager.sharedInstance.showLoading(self,color: UIColor.appColor())
        
        constrain(tableView) { (tableView) in
            tableView.top == (tableView.superview?.top)! + 1
            tableView.left == (tableView.superview?.left)! + 1
            tableView.right == (tableView.superview?.right)! - 1
            tableView.bottom == (tableView.superview?.bottom)! - 20
        }
        
        
        tableView.register(HomeCellTypeOne.self, forCellReuseIdentifier: HOME_CELL_TYPE_ONE_ID)
        tableView.register(HomeCellTypeTwo.self, forCellReuseIdentifier: HOME_CELL_TYPE_TWO_ID)
        tableView.register(BookmarkCell.self, forCellReuseIdentifier: BOOKMARK_CELL_ID)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
                
        viewModel?.delegate = self
        configureSubviews()
        bindViewModel(true) //first time binding
        
        //Observe change in news source
        
        NotificationCenter.default.reactive.notification(name:  NSNotification.Name(rawValue: "newsSourceChanged")).observeNext { (noti) in
            if let info = noti.userInfo as? Dictionary<String,AnyObject>{
                if let vm = info["data"]{
                    self.viewModel?.filterViewModel = vm as! NewsSourceViewModel
                }
            }
            self.viewModel?.observableIds.value.removeAll()
            self.viewModel?.observableFeeds.removeAll()
            self.tableView.reloadData()
            self.bindViewModel(true)
            }.dispose(in: bag)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //Analytics
        let tracker = GAI.sharedInstance().defaultTracker
        guard let _vm = viewModel else { return }
        tracker?.set(kGAIScreenName, value: _vm.name)
        let builder = GAIDictionaryBuilder.createScreenView()
        _ = builder?.set("start", forKey: kGAISessionControl)
        tracker?.send(builder?.build() as! [AnyHashable: Any])
        
        bindViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    func refresh(){
        
        //bindViewModel()
        viewModel?.refresh()
        refreshControl.endRefreshing()
    }
    
    
    /**
     Bind view model to controller
     
     - parameter first: true if first time binding
     */
    fileprivate func bindViewModel(_ first: Bool = false){
        
      
        if (appDataObject!.isOffline == false){
            viewModel?.refreshFeedsId(first)
        
        }else {
            viewModel?.offlineLoad()
            
            AlertViewManager.sharedInstance.stopLoading()
        }
        self.refreshControl.endRefreshing()
 
    }
    
    
    func configureSubviews(){
        
        tableView.delegate = self
        tableView.dataSource = self
    
        tableView.showsVerticalScrollIndicator = false
        tableView.alwaysBounceVertical = false
        
        tableView.estimatedRowHeight = APP_HEIGHT/5
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        
        
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        scrollToTopButton.setImage(UIImage(named:ImageName.ScrollToTopIcon.description), for: UIControlState())
        scrollToTopButton.center = offScreenCenter
        
        _ = scrollToTopButton.reactive.tap.observeNext {
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
    }
}




