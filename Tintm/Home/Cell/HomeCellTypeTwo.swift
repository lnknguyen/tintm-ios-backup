//
//  HomeCellTypeTwo.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/27/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography
import Kingfisher

@IBDesignable
class HomeCellTypeTwo: BaseHomeCell{
    var ratio : CGFloat = 0.65
    weak var tableView : UITableView?
    var didLayout  = false
    typealias HomeCellPresentable = TextPresentable & ImagePresentable
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: HOME_CELL_TYPE_TWO_ID)
        //self.addSubview(summaryView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    //initial constraint
    //Param type: 0 if home view, 1 if trend view
    override func constrainSubview(_ type: Int){
        //capture constraint and store into group
        if (type == 0){
            constrain(imgView, titleView, sourceView, timestampView, replace: group) { (view1, view2, view3, view4) in
                guard let superview = view1.superview else { return }
                view1.top == superview.top
                view1.left == superview.left
                view1.right == superview.right
                (view1.height == view1.width * ratio) ~ 800
                
                view2.top == view1.bottom + 5
                view2.left == view1.left + 10
                view2.right == view1.right - 10
                
                view3.top == view2.bottom + 5
                view3.left == view2.left
                
                view4.top == view3.top
                view4.left == view3.right + 5
                
                view3.bottom <= superview.bottom - 5
                view4.bottom == view3.bottom
                
            }
        } else {
            self.addSubview(summaryView)
            constrain(imgView, titleView, summaryView, replace: group) { (view1, view2, view3) in
                guard let superview = view1.superview else { return }
                view1.top == superview.top
                view1.left == superview.left
                view1.right == superview.right
                (view1.height == view1.width * ratio) ~ 800
                
                view2.top == view1.bottom + 5
                view2.left == view1.left + 10
                view2.right == view1.right - 10
                
                view3.top == view2.bottom + 5
                view3.left == view2.left
                view3.right == view2.right
                view3.bottom <= superview.bottom - 5
            }
            
        }
        
    }
    
    
    override func publishData(_ feed: HomeNewsEntity) {
        super.publishData(feed)
        self.constrainSubview(0)
        imgView.image = UIImage(named: ImageName.AppIcon.description)
        //imgView.kf_showIndicatorWhenLoading = true
        imgView.contentMode = .scaleToFill
        if let url = URL(string: feed.image) {
            
            imgView.kf.setImage(with: url, placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: { (receivedSize, totalSize) in
                
                }, completionHandler: {[unowned self] (image, error, cacheType, imageURL) in
                    if error == nil {
                        
                        guard let _ = image else {
                            // self.imgView.image = UIImage(named: ImageName.AppIcon.description)
                            return
                        }
                    }
                    else {
                        print("Error setting image")
                        self.imgView.image = UIImage(named: ImageName.AppIcon.description)
                    }
                    
                })
        }
    }
    //data from entity of home news
    override func publishData(_ feed: TrendsEntity){
        super.publishData(feed)
        self.constrainSubview(1)
        imgView.image = UIImage(named: ImageName.AppIcon.description)
        //imgView.kf_showIndicatorWhenLoading = true
        imgView.contentMode = .scaleToFill
        if let url = URL(string: feed.image) {
            
            imgView.kf.setImage(with: url, placeholder: nil, options: [.transition(ImageTransition.fade(1))], progressBlock: { (receivedSize, totalSize) in
                
                }, completionHandler: {[unowned self] (image, error, cacheType, imageURL) in
                    if error == nil {
                        
                        guard let _ = image else {
                            // self.imgView.image = UIImage(named: ImageName.AppIcon.description)
                            return
                        }
                    }
                    else {
                        print("Error setting image")
                        self.imgView.image = UIImage(named: ImageName.AppIcon.description)
                    }
                    
                })
        }
        
    }
    
    override func publishData(_ feed: RealmHomeObject) {
        super.publishData(feed)
    }
    
    
    override func configureSubview(_ presenter: HomeCellPresentable) {
        super.configureSubview(presenter)
        
    }
}
