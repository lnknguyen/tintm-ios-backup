//
//  HomeCellTypeTwo.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/26/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography
import Bond



class BaseHomeCell: UITableViewCell{
    
    
    weak var delegate : ButtonDelegate?
    var viewModel = HomeViewModel()
    var state = CellState.collapsed
    var imgView  = UIImageView()
    var titleView = UILabel()
    var summaryView = UILabel()
    var sourceView = UILabel()
    var timestampView = UILabel()
    var expandButton = UIButton()
    var group = ConstraintGroup()
    //var imageRequest : ImageRequest?
    
    typealias HomeCellPresentable = TextPresentable & ImagePresentable
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "")
        self.selectionStyle = .none
        
        self.addSubview(imgView)
        
        self.addSubview(titleView)
        self.addSubview(sourceView)
        self.addSubview(timestampView)
        
        configureSubview(viewModel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgView.image = nil
        titleView.text = ""
        sourceView.text = ""
        timestampView.text = ""
        summaryView.attributedText = NSMutableAttributedString(string: "")
        
        delegate = nil
        if state == .expanded {
            collapseView()
        }
    }
    
    func constrainSubview(_ type: Int){
     //   preconditionFailure("Subclass must implement this method")
    }
    
    func constrainSubview(){
      //  preconditionFailure("Subclass must implement this method")
    }
    func configureSubview(_ presenter: HomeCellPresentable){
        //Set subviews background color to white to avoid blending operation in cell
        //See more: https://medium.com/ios-os-x-development/perfect-smooth-scrolling-in-uitableviews-fd609d5275a5#.ymbe3iumc
        
        titleView.font = presenter.headingFont
        summaryView.font = presenter.summaryFont
        sourceView.font = presenter.sourceNameFont
        timestampView.font = presenter.timeStampFont
        
        sourceView.textColor = presenter.sourceNameColor
        timestampView.textColor = presenter.timeStampColor
        
        for subview in self.subviews{
            if subview is UILabel{
                if let subview = subview as? UILabel
                {
                    subview.numberOfLines = 0
                    subview.lineBreakMode = .byTruncatingTail
                    subview.sizeToFit()
                    subview.backgroundColor = UIColor.white
                }
            }
        }
        
        summaryView.numberOfLines = 0
        summaryView.lineBreakMode = .byTruncatingTail
        summaryView.sizeToFit()
        summaryView.backgroundColor = UIColor.white
        
        imgView.backgroundColor = UIColor.appLightGrayColor()
        //imgView.layer.borderWidth = 1.0
        imgView.layer.borderColor = UIColor.silverColor().cgColor
        
        expandButton.setImage(UIImage(named: ImageName.ExpandIcon.description), for: UIControlState())
        expandButton.imageView!.backgroundColor = UIColor.clear
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(20, APP_WIDTH-40, 5, 15)
        
        _ = expandButton.reactive.tap.observeNext {
            if let _delegate = self.delegate{
                _delegate.didTapButtonInsideCell(self)
            }
        }
    }
    
    //data from entity of home news
    func publishData(_ feed: HomeNewsEntity){
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        let summaryAttributedString = NSMutableAttributedString(string: feed.summary)
        summaryAttributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, summaryAttributedString.length))
        let titleAttributedString = NSMutableAttributedString(string: feed.title)
        titleAttributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, titleAttributedString.length))
        
        
        timestampView.text = feed.timestamp.timePassed
        titleView.attributedText = titleAttributedString
        summaryView.attributedText = summaryAttributedString
        sourceView.text = feed.sourceName
    }
    
    //data from entity of trend
    func publishData(_ feed: TrendsEntity){
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
       let titleAttributedString = NSMutableAttributedString(string: feed.title)
        titleAttributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, titleAttributedString.length))
        titleView.attributedText = titleAttributedString
        summaryView.text = feed.description
    }
    
    //data from realm db
    func publishData(_ feed: RealmHomeObject){
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        let summaryAttributedString = NSMutableAttributedString(string: feed.summary)
        summaryAttributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, summaryAttributedString.length))
        let titleAttributedString = NSMutableAttributedString(string: feed.title)
        titleAttributedString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, titleAttributedString.length))
        timestampView.text = feed.timestamp.timePassed
        titleView.attributedText = titleAttributedString
        summaryView.attributedText = summaryAttributedString
        sourceView.text = feed.sourceName
    }
    
    func expandView() {
        state.switchState()
        expandButton.setImage(UIImage(named: ImageName.CollapseIcon.description), for: UIControlState())
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(20, APP_WIDTH-40, 5, 15)
    }
    
    
    func collapseView() {
        state.switchState()
        expandButton.setImage(UIImage(named: ImageName.ExpandIcon.description), for: UIControlState())
        expandButton.imageEdgeInsets = UIEdgeInsetsMake(25, APP_WIDTH-40, 5, 15)
        
    }
}
