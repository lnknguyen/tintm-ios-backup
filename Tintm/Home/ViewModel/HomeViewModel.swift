//
//  HomeViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import PromiseKit
import Bond
import RealmSwift

enum HomeType {
    case trend
    case category
}

protocol HomeAdapter: class {
    /* Handler for id refreshing success*/
    func successRefreshIds(_ res: [Int], first: Bool )
    /* Handler for successfully load more feeds*/
    func successLoadMore(_ res: [HomeNewsEntity])
    func successLoadFeed(_ res: HomeNewsEntity)
    func successHandler(_ res: HomeNewsEntity, cell: BaseHomeCell)
    /* Handler for refresh realm database*/
    func successRefreshRealm(_ res: RealmHomeObject, cell: BaseHomeCell)
    /* Handler for success pulll to refresh*/
    func successPullToRefresh(_ count: Int)
    /* Handler for loading trend*/
    func successLoadTrends(_ res: [TrendsEntity])
    
    /* Handler for exception failure*/
    func failureHandler(_ err: Error)
    func failureLoadMore(_ err: Error)
}

class HomeViewModel:TextPresentable, ImagePresentable{
    
    var id: Int //category's id
    var name = "" // category's name
    let realmManager = RealmCommandManager()
    var observableFeeds : MutableObservableArray<HomeNewsEntity>
    
    var observableIds : Observable<[Int]>{
        didSet{
            print("old:",oldValue)
        }
    }
    var filterViewModel = NewsSourceViewModel()
    //Trend entities array
    var observableTrends: MutableObservableArray<TrendsEntity>
    
    //Indicate whether this is a home view or trend view
    var type : HomeType
    
    weak var delegate : HomeAdapter?
    
    var offlineDataSource = [RealmHomeObject]()
    
    init(id: Int = 0){
        self.id = id
        observableFeeds = MutableObservableArray()
        observableIds = Observable([])
        observableTrends = MutableObservableArray()
        type = self.id == 12 ? HomeType.trend :  HomeType.category //Trend view if id = 12, category otherwise
        filterViewModel.publishDataSource()
    }
    
    convenience init(id: Int, name: String){
        self.init(id: id)
        self.name = name
        
        
    }
    
    
    /**
     Refresh list of news id
     */
    func refreshFeedsId(_ first: Bool = false){
        switch type {
        case .category:
            NetworkManager.sharedInstance.getFeedsId(id) { (res) in
                switch res.status{
                case .success:
                    if let value = res.res{
                        self.observableIds.next(value)
                        
                        //self.observableIds = value
                        self.delegate?.successRefreshIds(value,first: first)
                    }
                    else {
                        self.delegate?.failureHandler(ErrorCode.unknownError)
                    }
                case .failure:
                    self.delegate?.failureHandler(res.error!)
                }
            }
            break
        case .trend:
            NetworkManager.sharedInstance.getCurrentTrends({ (res) in
                switch res.status{
                case .success:
                    if let value = res.res{
                        self.observableTrends.insert(contentsOf: value, at: self.observableTrends.endIndex)
                        self.delegate?.successLoadTrends(value)
                    }
                case .failure:
                    self.delegate?.failureHandler(res.error!)
                }
                
            })
            break
        }
    }
    
    /**
     Get a single feed
     */
    func getFeed(_ feedId: Int, cell: BaseHomeCell){
        NetworkManager.sharedInstance.getFeeds(feedId) { (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    let realmNews = self.realmManager.create(value)
                    self.realmManager.write(realmNews)
                    self.delegate?.successRefreshRealm(realmNews as! RealmHomeObject,cell: cell)
                }
            case .failure:
                if let err = res.error{
                    print(err)
                    self.delegate?.failureHandler(err)
                }
                
            }
        }
    }
    
    
    func refresh(){
        
        NetworkManager.sharedInstance.getFeedsId(id) { (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    let insert = value.filter({ (val) -> Bool in
                        !self.observableIds.value.contains(val)
                    })
                    self.observableIds.next(value)
                    guard insert.count > 0 else { return }
                    self.reloadAfterRefresh(insert)
                }
                else {
                    self.delegate?.failureHandler(ErrorCode.unknownError)
                }
            case .failure:
                self.delegate?.failureHandler(res.error!)
            }
        }
    }
    
    // Reload whole table view after pull to refresh
    func reloadAfterRefresh(_ values: [Int]){
        
        NetworkManager.sharedInstance.getMoreFeeds(observableIds.value, offset: values.count, count: values.count) { (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    let arr = value.filter({ (entity) -> Bool in
                        //Filter source news configuration
                        //Extract names of available source
                        let temp = self.filterViewModel.dataSource.filter({ (val) -> Bool in
                            return val.status == true
                        }).map({ (val) -> String in
                            return val.alias
                        })
                        //Filter source
                        return temp.contains(entity.sourceName)
                    })
                    self.observableFeeds.insert(contentsOf: arr, at: 0)
                    guard values.count > 0 else { return }
                    self.delegate?.successPullToRefresh(values.count)
                }
            case .failure:
                if let err = res.error{
                    self.delegate?.failureLoadMore(err)
                }
            }
        }
    }
    
    /**
     Load 10 more feeds
     
     - parameter offset: index to begin from
     */
    func loadMore(_ offset: Int){
        NetworkManager.sharedInstance.getMoreFeeds(observableIds.value, offset: offset) { [unowned self] (res) in
            switch res.status{
            case .success:
                if let value = res.res{
                    let arr = value.filter({ (entity) -> Bool in
                        //Filter source news configuration
                        //Extract names of available source
                        let temp = self.filterViewModel.dataSource.filter({ (val) -> Bool in
                            return val.status == true
                        }).map({ (val) -> String in
                            return val.alias
                        })
                        //Filter source
                        return temp.contains(entity.sourceName)
                    })
                    
                    self.observableFeeds.insert(contentsOf: arr, at: self.observableFeeds.endIndex)
                    self.delegate?.successLoadMore(arr)
                }
            case .failure:
                if let err = res.error{
                    self.delegate?.failureLoadMore(err)
                }
            }
        }
    }
    /**
     Offline loading
     */
    func offlineLoad(){
        offlineDataSource = realmManager.read(.home, dictionary: ["categoryId":id as AnyObject]) as! [RealmHomeObject]
        offlineDataSource.sort(by: {$0.0.timestamp.toDate().compare($0.1.timestamp.toDate() as Date) == .orderedDescending})
    }
    
    //Filter source
    func filterSource(){
        //Empty source
        self.observableFeeds.removeAll()
        //Load more from beginning
        loadMore(0)
    }
    
}




