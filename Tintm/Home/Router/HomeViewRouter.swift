//
//  HomeViewRouter.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

protocol HomeViewRouterProtocol {
    func pushFromHomeViewToArticle(_ row: Int)
    func pushFromHomeViewToPageView(_ row: Int)
    func pushFromHomeViewToTrendView(_ row: Int)
}

