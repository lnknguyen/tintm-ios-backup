//
//  ArticleViewRouter.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/29/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

protocol ArticleViewRouterProtocol {
    func popFromArticleViewToHomeView()
    func popFromArticleViewToBookmarkView()
    func pushFromArticleViewToRelatedArticleView(_ row: Int)
}

