//
//  File.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/22/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography
import Bond
import WebKit
import RealmSwift
import Foundation
import Google

//Remember to target delegate functions in viewmodel to view controller
class ArticleViewController: UIViewController{
    var viewModel = ArticleViewModel()
    var router : ArticleViewRouter?
    var webView = UIWebView()
    var tableView = UITableView(frame: CGRect.zero, style: .plain)
    var contentHeight = Observable<CGFloat>(0.0)
    let realmManager = RealmCommandManager()
    var contentOffset : CGPoint = CGPoint.zero
    let bundle = Bundle.main
    var path = ""
    var jsPath = ""
    let bookmarkButton = UIButton(frame: CGRect(x: 0,y: 0,width: 35,height: 35))
    let shareButton = UIButton(frame: CGRect(x: 0,y: 0,width: 35,height: 35))
    
    
    var backFromPop = false
    let tracker = GAI.sharedInstance().defaultTracker
    typealias ArticleViewPresenter = TextPresentable & ImagePresentable
    
    //MARK: -Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationBar()
        view.backgroundColor = UIColor.white
        tableView.delegate = self
        tableView.dataSource = self
        
        path =  bundle.path(forResource: "default", ofType: "css")! // css path
        jsPath = bundle.path(forResource: "article", ofType: "js")! // js path
        configureSubviews()
        constrainSubviews()
        webView.scrollView.delegate = self
        viewModel.delegate = self // -> important
        bindViewModel()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.automaticallyAdjustsScrollViewInsets = false
        //configureNavigationBar()
        //webView.scrollView.setContentOffset(contentOffset, animated: false)
        
        webView.addObserver(self, forKeyPath: "loading", options: .new, context: nil)
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        webView.scrollView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        //bindViewModel()
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        contentOffset =  webView.scrollView.contentOffset
        
        super.viewWillDisappear(true)
        backFromPop = true
        AlertViewManager.sharedInstance.stopLoading()
        if let news = viewModel.realmNews {
            viewModel.realmManager.write(news)
        }
        
        webView.removeObserver(self, forKeyPath: "loading")
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
        webView.scrollView.removeObserver(self, forKeyPath: "contentSize")
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func constrainSubviews(){
        view.addSubview(webView)
        webView.scrollView.addSubview(tableView)
        
        constrain(webView){ webView in
            guard let superview = webView.superview else {return}
            webView.top == superview.top
            webView.left == superview.left
            webView.right == superview.right
            (webView.bottom == superview.bottom - 5) ~ LayoutPriority(600)
        }
        
        
        
    }
    
    //Private functions
    fileprivate func configureSubviews(){
        view.backgroundColor = UIColor.clear
        
        
        tableView.backgroundColor = UIColor.red
        //Enable zooming in webview
        webView.scrollView.minimumZoomScale = 1.0
        webView.scrollView.maximumZoomScale = 2.0
        
        webView.delegate = self
        
        webView.scrollView.scrollsToTop = false
        webView.scrollView.bounces = false
        webView.scrollView.clipsToBounds = true
        if #available(iOS 9.0, *) {
            webView.allowsLinkPreview = true
        } else {
            // Fallback on earlier versions
        }
        
        tableView.register(RelatedNewsCell.self, forCellReuseIdentifier: RELATED_NEWS_ID)
        tableView.estimatedRowHeight = APP_HEIGHT/5
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        
    }
    
    //View model binding
    func bindViewModel(){
        buttonHandlers()
        
        let news = viewModel.realmManager.read(.news, dictionary: ["id":viewModel.id as AnyObject]) as! [RealmNewsObject]
        if (news.count > 0){
            if let news = news.first{
                
                viewModel.realmNews = news
                
                webView.loadHTMLString(news.content, baseURL: URL(fileURLWithPath: self.path))
                if let obj = news.homeObject{
                    self.viewModel.url.value = obj.tintmUrl
                    self.viewModel.isBookmarked.next(obj.isBookmark)
                }
                // constrainSubviews()
            }
        }
        else
        {
            viewModel.loadNewsById()
        }
        AlertViewManager.sharedInstance.showLoading(self, color: UIColor.appColor())
        
        
        viewModel.loadRelatedFeeds()
        
        
        
    }
    
    //Configure navigation bar buttons
    fileprivate func configureNavigationBar(){
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        let backbutton = UIButton(frame: CGRect(x: 0,y: 0,width: 50,height: 35))
        backbutton.imageEdgeInsets = UIEdgeInsetsMake(5, -15, 5,35)
        backbutton.setImage(UIImage(named: ImageName.BackIcon.description), for: UIControlState())
        _ = backbutton.reactive.tap.observeNext {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        
        let button_1 = UIBarButtonItem(customView: backbutton)
        
        self.navigationItem.leftBarButtonItems = [button_1]
        
        bookmarkButton.imageView?.frame = CGRect(x: 0,y: 0,width: 35,height: 35)
        bookmarkButton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        bookmarkButton.imageView!.contentMode = .scaleAspectFit
        let bookmark = UIBarButtonItem(customView: bookmarkButton)
        
        
        shareButton.imageView?.frame = CGRect(x: 0,y: 0,width: 35,height: 35)
        shareButton.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 2, 5)
        shareButton.imageView!.contentMode = .scaleAspectFit
        shareButton.setImage(UIImage(named: ImageName.ShareIcon.description), for: UIControlState())
        let share = UIBarButtonItem(customView: shareButton)
        
        self.navigationItem.rightBarButtonItems = [bookmark,share]
    }
    
    //Observe change in content height and loading status
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let path = keyPath else { return }
        if ( path == "contentSize"){
            
            let height = webView.scrollView.contentSize.height
            
            tableView.frame = CGRect(x: 0, y: height, width: APP_WIDTH, height: APP_HEIGHT)
            webView.scrollView.contentMode = .scaleAspectFit
            
            var contentHeight : CGFloat = 0.0
            _ = viewModel.relatedFeeds.observeNext(with: { [unowned self] (value) in
                if value.count > 0{
                    contentHeight = self.tableView.contentSize.height
                }
            })
            
            
            webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, contentHeight, 0)
            
            
            
        }
    }
    
    //handling buttons' actions
    fileprivate func buttonHandlers(){
        let nonBookmarkedImg = UIImage(named: ImageName.DisabledBookmarkIcon.description)
        let bookmarkedImg = UIImage(named: ImageName.EnabledBookmarkIcon.description)
        
        var url : URL?
        
        viewModel.isBookmarked.observeNext(with: {[unowned self] (value) in
            
            if value {
                self.bookmarkButton.setImage(bookmarkedImg, for: .normal)
            }
            else {
                self.bookmarkButton.setImage(nonBookmarkedImg, for: .normal)
            }
            
        }).dispose(in: bag)
        
        var sent = false;
        
        viewModel.url.observeNext(with: { (text) in
            url = NSURL(string: text)! as URL
            if (!sent && text != ""){
                //Analytics
                let builder = GAIDictionaryBuilder.createScreenView()
                _ = builder?.set("start", forKey: kGAISessionControl)
                self.tracker?.set(kGAIScreenName, value: text)
                self.tracker?.send(builder?.build() as! [AnyHashable: Any])
                sent = true;
            }
            
        }).dispose(in: bag)
        
        
        
        _ = shareButton.reactive.tap.observeNext {
            let rootVc = UINavigationController.rootNavigationController
            let accVc = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            rootVc.present(accVc, animated: true, completion: nil)
            
            }.dispose(in: bag)
        
        
        _ = bookmarkButton.reactive.tap.observeNext {[unowned self] in
            if (self.bookmarkButton.currentImage == nonBookmarkedImg){
                self.bookmarkButton.setImage(bookmarkedImg, for: .normal)
                AlertViewManager.sharedInstance.showSuccess(.bookmarked)
                self.realmManager.update{
                    self.viewModel.realmNews?.homeObject!.isBookmark = true
                }
                
            }else{
                self.bookmarkButton.setImage(nonBookmarkedImg, for: .normal)
                self.realmManager.update{
                    self.viewModel.realmNews?.homeObject!.isBookmark = false
                }
            }
            
            }.dispose(in: bag)
    }
}



