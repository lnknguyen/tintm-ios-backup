//
//  ArticleViewControllerExtension.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/11/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import WebKit
import Cartography
import Google

extension ArticleViewController: UIWebViewDelegate{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        AlertViewManager.sharedInstance.stopLoading()

    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if (navigationType==UIWebViewNavigationType.linkClicked){
            let url = request.url
            let webView = UIWebView(frame: CGRect(x: 0, y: 0, width: APP_WIDTH, height: APP_HEIGHT))
            let vc = UIViewController()
            vc.view.addSubview(webView)
            webView.loadRequest(URLRequest(url: url!))
            navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            navigationController?.pushViewController(vc, animated: true)
            
        }
        return true
    }
}

//MARK: View model delegate
extension ArticleViewController: ArticleAdapter{
    
    func successHandler(){
        
        let entity = viewModel.feed.value
        
        let content = entity.content
        let title = entity.title
        let source = entity.sourceName
        let timestamp = entity.timestamp
        
        //Convert to html string
        let desHtml = String.toHtml(title, sourceName: source,timeStamp: timestamp, content: content)
        webView.loadHTMLString(desHtml, baseURL: URL(fileURLWithPath: path))
        
        let realmObj = viewModel.realmManager.create(entity)
        viewModel.realmManager.write(realmObj)
        viewModel.realmNews = realmObj as? RealmNewsObject
        
        //webView.scrollView.fadeIn(0.5, delay: 0.0)
        viewModel.url.next(entity.tintmUrl)
    }
    
    func failureHandler(_ err: Error){
        AlertViewManager.sharedInstance.showFailure(.noInternetConnection)
        AlertViewManager.sharedInstance.stopLoading()
    }
    
    //Handlers for load more operation
    func successLoadMoreHandler() {
        
        tableView.reloadData()
    }
    
    func failureLoadMore(_ err: Error) {
        print("No related feeds")
    }
}

//MARK: Scrollview delegate
extension ArticleViewController: UIScrollViewDelegate{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollView.decelerationRate = UIScrollViewDecelerationRateNormal
    }
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        return false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
        
}

//MARK: Tableview delegate and data source
extension ArticleViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.relatedFeeds.value.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let inpRow = indexPath.row
        let cell = (tableView.dequeueReusableCell(withIdentifier: RELATED_NEWS_ID, for: indexPath) as? RelatedNewsCell)!
        let entity = viewModel.relatedFeeds.value[inpRow]
        
        cell.publishData(entity)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if viewModel.relatedFeeds.value.count > 0{
            return "Xem thêm"
        }
        else {return nil}
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Analytics
        
        let event = GAIDictionaryBuilder.createEvent(
            withCategory: "article_event",
            action: "tapped",
            label: "related_cell",
            value: nil).build()
        tracker?.send(event as! [AnyHashable: Any])
        
        if let ye = router {
            ye.pushFromArticleViewToRelatedArticleView(indexPath.row)
        }else {
            print("nil router")
        }
    }
    
}
