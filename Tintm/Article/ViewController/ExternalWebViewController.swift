//
//  ExternalWebViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 10/6/16.
//  Copyright © 2016 Vu Nguyen. All rights reserved.
//



import UIKit
import Cartography
import Bond
import WebKit
import RealmSwift
import Foundation
import Google

//Remember to target delegate functions in viewmodel to view controller
class ExternalViewController: UIViewController{
    
    var webView = WKWebView(frame: CGRect(x: 0, y: 0, width: APP_WIDTH, height: APP_HEIGHT))
    let progressBar = UIProgressView(progressViewStyle: .default)
    var url = URL(fileURLWithPath: "")
    let wkUserContentController = WKUserContentController()
    //MARK: -Lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    init(url: URL){
        super.init(nibName: nil, bundle: nil)
        self.url = url
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        configureSubviews()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        configureNavigationBar()
        
        webView.addObserver(self, forKeyPath: "loading", options: .new, context: nil)
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(true)
        AlertViewManager.sharedInstance.stopLoading()
        
        webView.removeObserver(self, forKeyPath: "loading")
        webView.removeObserver(self, forKeyPath: "estimatedProgress")
        
        wkUserContentController.removeAllUserScripts()
        
    }
    
    //Private functions
    fileprivate func configureSubviews(){
        
        
        if #available(iOS 9.0, *) {
            webView.allowsLinkPreview = true
        } else {
            // Fallback on earlier versions
        }
        
        
        
        //Enable zooming in webview
        var scriptContent = "var meta = document.createElement('meta');"
        scriptContent += "meta.name='viewport';"
        scriptContent += "meta.content='width=device-width,initial-scale=0.3, maximum-scale=2.0, user-scalable=2.0';"
        scriptContent += "document.getElementsByTagName('head')[0].appendChild(meta);"
        webView.scrollView.minimumZoomScale = 1.0
        webView.scrollView.maximumZoomScale = 2.0
        let uScript = WKUserScript(source: scriptContent, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        wkUserContentController.addUserScript(uScript)
        let config = WKWebViewConfiguration()
        config.userContentController = wkUserContentController
        webView = WKWebView(frame: CGRect(x: 0, y: 0, width: APP_WIDTH, height: APP_HEIGHT), configuration: config)
    
        progressBar.frame = CGRect(x: 0, y: 0, width: APP_WIDTH, height: 25)
        progressBar.tintColor = UIColor.blue
        
        view.addSubview(webView)
        view.addSubview(progressBar)
        webView.load(URLRequest(url: url))
        
    }
    
    
    fileprivate func configureNavigationBar(){
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        let backbutton = UIButton(frame: CGRect(x: 0,y: 0,width: 50,height: 35))
        backbutton.imageEdgeInsets = UIEdgeInsetsMake(5, -15, 5,35)
        backbutton.setImage(UIImage(named: ImageName.BackIcon.description), for: UIControlState())
        _ = backbutton.reactive.tap.observeNext {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        
        let button_1 = UIBarButtonItem(customView: backbutton)
        self.navigationItem.leftBarButtonItems = [button_1]
        
        
    }
    
    //Observe change in content height and loading status
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let path = keyPath else { return }
        if path == "estimatedProgress"{
            if let webView = object as? WKWebView
            {
                self.progressBar.progress = Float(webView.estimatedProgress)
                
                if (webView.estimatedProgress == 1.0){
                    self.progressBar.isHidden = true
                } else {
                    self.progressBar.isHidden = false
                }
            }
            
        }
    }
    
}
