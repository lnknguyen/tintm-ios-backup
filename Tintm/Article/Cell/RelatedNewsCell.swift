//
//  RelatedNewsCell.swift
//  Tintm
//
//  Created by Nguyen Luong on 8/17/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit

class RelatedNewsCell : HomeCellTypeOne {
    var presenter = ArticleViewModel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: RELATED_NEWS_ID)
        expandButton.isHidden = true
        expandButton.isEnabled = false
        expandButton.frame = CGRect.zero
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
