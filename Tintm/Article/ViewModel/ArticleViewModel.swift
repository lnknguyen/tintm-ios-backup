//
//  ArticleViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/22/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import Bond
import Social

protocol ArticleAdapter: class{
    func successHandler()
    func successLoadMoreHandler()
    func failureHandler(_ err: Error)
    func failureLoadMore(_ err: Error)
}

class ArticleViewModel: ButtonDelegate{
    var id: Int = 0
    var feed : Observable<NewsEntity>
    let realmManager = RealmCommandManager()
    var realmNews : RealmNewsObject? = nil
    weak var delegate : ArticleAdapter?
    var relatedFeeds : Observable<[HomeNewsEntity]>
    
    var url : Observable<String>
    var isBookmarked : Observable<Bool>
    
    init(id: Int = 0){
        self.id = id
        feed = Observable(NewsEntity())
        relatedFeeds = Observable([])
        url = Observable("")
        isBookmarked = Observable(false)
    }
    
    
    func shareButtonHandler(_ button: UIButton, url: URL){
        _ = button.reactive.tap.observeNext {
            let rootVc = UINavigationController.rootNavigationController
            let accVc = UIActivityViewController(activityItems: [url], applicationActivities: nil)
            rootVc.present(accVc, animated: true, completion: nil)
        }
    }
    
    //MARK: -Networking
    func loadNewsById(){
        NetworkManager.sharedInstance.getFeedDetail(id) { (res) in
            switch res.status{
            case .success:
                if let result = res.res{
                    self.feed.next(result)
                }
                self.delegate?.successHandler()
            case .failure:
                if let err = res.error{
                    self.delegate?.failureHandler(err)
                }
            }
        }
    }
    
    func loadRelatedFeeds(){
        NetworkManager.sharedInstance.getRelatedFeeds(id) { (res) in
            switch res.status {
            case .success:
                if let result = res.res{
                    self.relatedFeeds.next(result)
                    self.delegate?.successLoadMoreHandler()
                }
            case .failure:
                if let err = res.error{
                    self.delegate?.failureLoadMore(err)
                }
            }
            
        }
    }
}



//Text presentable, img presentable
extension ArticleViewModel: TextPresentable,ImagePresentable{
    var align : NSTextAlignment { return NSTextAlignment.center }
    var headingFont : UIFont { return UIFont(name: "Roboto-Bold", size: 20)!}
    var imageName : String { return ImageName.CancelIcon.description}
}

