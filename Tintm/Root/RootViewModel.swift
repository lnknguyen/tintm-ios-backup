//
//  RootViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//




import PromiseKit
import Bond

protocol RootAdapter: class {
    func successHandler()
    func failureHandler()
}


struct RootViewModel: TableViewPresentable{
    var categoryArray:  Observable<[CategoryEntity]>
    weak var delegate : RootAdapter?
    let realmManager = RealmCommandManager()
    var vcArray = [UIViewController]()
    
    init(){
        categoryArray = Observable([])
    }
    
    //MARK: -Networking
    
    func load(){
        NetworkManager.sharedInstance.getCategories { (res)  in
            switch res.status{
            case .success:
                if let arr = res.res{
                    for val in arr{
                        var tempVal = val
                        if tempVal.id == 4 {
                            tempVal.name = "KH-CN"
                        }
                        let obj = self.realmManager.create(tempVal)
                        self.realmManager.write(obj)
                    }
                    self.categoryArray.next(arr)
                }
                self.delegate!.successHandler()
            case .failure:
                self.delegate!.failureHandler()
            }
        }
    }
    
    mutating func handleOnline(){
        
        for entity in categoryArray.value{
            //Remove xu huong from category
            if (entity.id != 12 ){
                let viewModel = HomeViewModel(id: entity.id,name: entity.name)
                let vc = HomeViewController()
                let router = HomeViewRouter(vc: vc, vm: viewModel)
                vc.viewModel = viewModel
                if entity.id == 4 {
                    vc.title = "KH-CN"
                } else {
                    vc.title = entity.name
                }
                
                vc.view.backgroundColor = UIColor.white
                vc.router = router
                let navVc = UINavigationController(rootViewController: vc)
                navVc.isNavigationBarHidden = true
                vcArray.append(navVc)
            }
        }
        
    }
    
    mutating func handleOffline(_ objs: [RealmCategoryObject]){
        for entity in objs{
            if entity.id != 11 || entity.id != 12||entity.id != 13 {
                let viewModel = HomeViewModel(id: entity.id,name: entity.name)
                let vc = HomeViewController()
                let router = HomeViewRouter(vc: vc, vm: viewModel)
                vc.viewModel = viewModel
                
                if entity.id == 4 {
                    vc.title = "KH-CN"
                } else {
                    vc.title = entity.name
                }
                
                vc.view.backgroundColor = UIColor.white
                vc.router = router
                let navVc = UINavigationController(rootViewController: vc)
                navVc.isNavigationBarHidden = true
                vcArray.append(navVc)
                
                
            }
        }
        
    }
}
