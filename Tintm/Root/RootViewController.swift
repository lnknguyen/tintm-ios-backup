//
//  Root.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit


import Cartography

import RealmSwift

class RootViewController: UIViewController{
    var viewModel = RootViewModel()
    var pageMenu : CAPSPageMenu?
    
    let refreshControl = UIRefreshControl()
    let params: [CAPSPageMenuOption] = [.scrollMenuBackgroundColor(UIColor.appColor()),
                                        .centerMenuItems(true),
                                        .enableHorizontalBounce(true),
                                        .menuItemWidthBasedOnTitleTextWidth(true),
                                        .selectionIndicatorColor(UIColor.white),
                                        .selectedMenuItemLabelColor(UIColor.white),
                                        .menuItemFont(UIFont.appRegularFont(15)),
                                        .menuItemSeparatorRoundEdges(true)]
    
    
    let emptyView = StateView(frame: CGRect.zero, state: .noInternet)
    
    var menuLeft = UISideMenuNavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge()
        view.backgroundColor = UIColor.appColor()
        viewModel.delegate = self
        
        setupSideMenu()
        bindViewModel()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = true
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    fileprivate func bindViewModel(){
        AlertViewManager.sharedInstance.showLoading(self)
        viewModel.load()
    }
    
    func refresh(){
        bindViewModel()
    }
    
    fileprivate func setupSideMenu(){
        let vc = SideMenuViewController()
        let router = RootViewRouter(vc: vc)
        vc.router = router
        menuLeft.leftSide = true
        menuLeft.viewControllers = [vc]
        SideMenuManager.menuWidth = 80.0
        SideMenuManager.menuAnimationFadeStrength = 1
        SideMenuManager.menuShadowRadius = 20
        SideMenuManager.menuShadowColor = UIColor.appColor()
        SideMenuManager.menuAddPanGestureToPresent(toView: (self.navigationController?.navigationBar)!)
        SideMenuManager.menuPushStyle = .subMenu
        SideMenuManager.menuLeftNavigationController = menuLeft
        
    }
}

//MARK: Delegated by viewmodel
extension RootViewController : RootAdapter{
    //MARK: Result handlers
    func successHandler(){
        emptyView.removeFromSuperview()
        let realm = try! Realm()
        try! realm.write({
            appDataObject!.isOffline = false
        })
        AlertViewManager.sharedInstance.stopLoading()
        viewModel.handleOnline()
        setupPageMenu()
        
        
    }
    
    func failureHandler(){
        //Display a temporary table view
        //Scroll the table to try loading data
        let realm = try! Realm()
        try! realm.write({
            appDataObject!.isOffline = true
        })

        AlertViewManager.sharedInstance.showFailure(.noInternetConnection)
        let objs = viewModel.realmManager.read(.category, dictionary: [:]) as! [RealmCategoryObject]
        if objs.count <= 0{
            view.addSubview(emptyView)
            constrain(emptyView) { (view1) in
                guard let superview = view1.superview else { return }
                view1.top == superview.top
                view1.left == superview.left
                view1.right == superview.right
                view1.bottom == superview.bottom
            }
            
        }else {
            
            viewModel.handleOffline(objs)
            setupPageMenu()
            
        }
        
        AlertViewManager.sharedInstance.stopLoading()
        self.refreshControl.endRefreshing()
    }
    
    fileprivate func setupPageMenu(){
        pageMenu = CAPSPageMenu(viewControllers: self.viewModel.vcArray, frame: CGRect(x: 0, y: STATUS_BAR_HEIGHT, width: APP_WIDTH, height: APP_HEIGHT), pageMenuOptions: params)
        view.addSubview(self.pageMenu!.view)
        
        
        pageMenu?.menuButton.reactive.tap.observeNext { _ in
            self.present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }
    }
    
}

