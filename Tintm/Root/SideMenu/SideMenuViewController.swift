//
//  SideMenuViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/28/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Cartography
import Bond
class SideMenuViewController: UIViewController{
    let settingButton = UIButton()
    let bookmarkButton = UIButton()
    let searchButton = UIButton()
    var router : RootViewRouter?
    
    override func viewDidLoad(){
        self.navigationController?.isNavigationBarHidden = true
        
        view.addSubview(settingButton)
        view.addSubview(bookmarkButton)
        view.addSubview(searchButton)
        
        configureSubviews()
        constrainSubviews()
    }
    
    fileprivate func configureSubviews(){
        
        searchButton.backgroundColor = UIColor.white
        settingButton.backgroundColor = UIColor.white
        bookmarkButton.backgroundColor = UIColor.white
        
        searchButton.setImage(UIImage(named: ImageName.SearchIcon.description), for: UIControlState())
        settingButton.setImage(UIImage(named: ImageName.SettingIcon.description), for: UIControlState())
        bookmarkButton.setImage(UIImage(named: ImageName.BookmarkIcon.description), for: UIControlState())
        
        settingButton.contentMode = .scaleAspectFit
        bookmarkButton.contentMode = .scaleAspectFit
        searchButton.contentMode = .scaleAspectFit
        
        settingButton.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15)
        bookmarkButton.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15)
        searchButton.imageEdgeInsets = UIEdgeInsetsMake(15, 15, 15, 15)
        
        _ = bookmarkButton.reactive.tap.observeNext {[unowned self] in
            
            self.router?.pushFromSideMenuToBookmarkView()
        }
        
        _ = settingButton.reactive.tap.observeNext { [unowned self] in
            self.router?.pushFromSideMenuToSettingsView()
        }
        
        _ = searchButton.reactive.tap.observeNext { [unowned self] in
            self.router?.pushFromSideMenuToSearchView()
        }
    }
    
    fileprivate func constrainSubviews(){
        constrain(searchButton, settingButton, bookmarkButton) { (view1, view2, view3) in
            guard let superview = view1.superview else { return }
            view2.centerY == superview.centerY
            view2.centerX == superview.centerX
            view2.left == superview.left + 10
            view2.right == superview.right - 10
            view2.height == view2.width
            
            align(centerX: view2, view1,view3)
            
            view1.bottom == view2.top - 10
            view1.left == view2.left
            view1.right == view2.right
            view1.height == view1.width
            
            view3.top == view2.bottom + 10
            view3.left == view2.left
            view3.right == view2.right
            view3.height == view3.width
            
        }
    }
}
