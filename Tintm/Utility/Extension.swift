//
//  Extension.swift
//  Tintm
//
//  Created by Nguyen Luong on 6/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit


import Social


//MARK: Font extension
extension UIFont{
    
    class func appRegularFont(_ fontSize: CGFloat)->UIFont{
        return UIFont(name: "Roboto-Regular", size: fontSize)!
        
    }
    
    class func appBoldFont(_ fontSize: CGFloat)->UIFont{
        return UIFont(name: "Roboto-Bold", size: fontSize)!
    }
    
    class func appItalicFont(_ fontSize: CGFloat)->UIFont{
        return UIFont(name: "Roboto-Italic", size: fontSize)!
    }
    
    class func appSecondaryBoldFont(_ fontSize: CGFloat)->UIFont{
        return UIFont(name: "Georgia", size: fontSize)!
    }
}

//MARK: String extension
extension String{
    
    //Calculate time passed from timestamp
    var timePassed : String {
        let dateFormatter = DateFormatter()
        let localeStr = "en_US_POSIX"
        dateFormatter.locale = Locale(identifier: localeStr)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let date = dateFormatter.date(from: self)
        let currentDate = Date()
        
        let dayHourMinuteSecond: NSCalendar.Unit = [.day, .hour, .minute, .second]
        if let _date = date {
            let difference = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: _date, to: currentDate, options: [])
            
            let seconds = "\(String(describing: difference.second)) giây trước"
            let minutes = "\(String(describing: difference.minute)) phút trước"
            let hours = "\(String(describing: difference.hour))h trước "
            let days = "\(String(describing: difference.day)) ngày trước "
            
            if difference.day!    > 0 { return days }
            if difference.hour!   > 0 { return hours }
            if difference.minute! > 0 { return minutes }
            if difference.second! > 0 { return seconds }
        } else {
            print(self)
        }
        return ""
    }
    
    
    
    //Append header to content and convert to a html string
    //Extension method to convert a string to a html
    static func toHtml(_ title: String, sourceName: String, timeStamp: String, content: String) -> String{
        
        return "<html>\n<head>\n<meta charset=\"utf-8\"><link rel=\"stylesheet\" href=\"default.css\" ></head><body><div id=\"main\"><div tm-type=title>\(title)</div>\n<div class=\"inline-container\"><div id=\"source\">\(sourceName)</div><div id=\"timestamp\">\(timeStamp.timePassed)</div></div> \(content)</div></body>\n</html>"
    }
    
    //Extension method to convert a string to NSDate
    func toDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let date = dateFormatter.date(from: self){
            return date
        }
        print("Fail to convert string to date, returning current date instead")
        return Date()
    }
    
}

//MARK: Color extension
extension UIColor {
    class func appColor() -> UIColor{
        return UIColor(red: 179.0/255, green: 61.0/255, blue: 61.0/255, alpha: 1.0)
    }
    
    class func silverColor() -> UIColor{
        return UIColor(red: 192.0/255, green: 192.0/255, blue: 192.0/255, alpha: 1.0)
    }
    
    class func appLightGrayColor() -> UIColor{
        return UIColor(red: 211.0/255, green: 211.0/255, blue: 211.0/255, alpha: 1.0)
    }
}

//MARK: UIView extension
extension UIView {
    var originX : CGFloat{
        return self.frame.origin.x
    }
    
    var originY : CGFloat{
        return self.frame.origin.y
    }
    
    
    var selfWidth : CGFloat{
        return self.frame.size.width
    }
    
    var selfHeight : CGFloat{
        return self.frame.size.height
    }
}

//MARK: UINavigationController extension
extension UINavigationController {
    func customizeNavigationBar() {
        self.navigationBar.backgroundColor = UIColor.appColor()
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = UIColor.appColor()
        self.navigationBar.isTranslucent = true
        self.navigationBar.isOpaque = false
    }
    
    class var rootNavigationController : UINavigationController {
        return (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController)!
    }
}

//MARK: NSDate extension
extension Date {
    func isGreaterThan(_ date: Date, unit: NSCalendar.Unit, amount: Int) -> Bool{
        
        let dayHourMinuteSecond: NSCalendar.Unit = [.second,.minute, .day, .hour, .minute, .year]
        
        let difference = (Calendar.current as NSCalendar).components(dayHourMinuteSecond, from: self, to: date, options: [])
        
        switch unit{
        case NSCalendar.Unit.second:
            return difference.second! > amount
        case NSCalendar.Unit.minute:
            return difference.minute! > amount
        case NSCalendar.Unit.hour:
            return difference.hour! > amount
        case NSCalendar.Unit.day:
            return difference.day! > amount
        case NSCalendar.Unit.month:
            return difference.month! > amount
        case NSCalendar.Unit.year:
            return difference.year! > amount
        default:
            return false
        }
    }
    
    func isSmallerThan(_ date: Date, unit: NSCalendar.Unit, amount: Int) -> Bool {
        return !isGreaterThan(date, unit: unit, amount: amount)
    }

}

//Custom animations
extension UIView {
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
            }, completion: completion)
    }
}

//MARK: - Custom activity controller
extension UIActivityViewController {
    
    class func defaultActivityController(_ string: String, url: URL, sender: UINavigationController) -> UIActivityViewController {
        let vc = UIActivityViewController(activityItems: [string,url], applicationActivities: nil)
        vc.excludedActivityTypes = [UIActivityType.postToWeibo, UIActivityType.postToTencentWeibo, UIActivityType.postToTwitter]
        return vc
    }
    
    //Custom provider
    class CustomProvider : UIActivityItemProvider {
        
        var facebookMessage : String!
        var twitterMessage : String!
        var emailMessage : String!
        
        init(placeholderItem: AnyObject, facebookMessage : String, twitterMessage : String, emailMessage : String) {
            super.init(placeholderItem: placeholderItem)
            self.facebookMessage = facebookMessage
            self.twitterMessage = twitterMessage
            self.emailMessage = emailMessage
        }
        
        override var item : Any {
            switch (self.activityType!) {
            case UIActivityType.postToFacebook:
                return self.facebookMessage as AnyObject
            case UIActivityType.postToTwitter:
                return self.twitterMessage as AnyObject
            case UIActivityType.mail:
                return self.emailMessage as AnyObject
            default:
                return "Any Message" as AnyObject
            }
        }
    }
}


