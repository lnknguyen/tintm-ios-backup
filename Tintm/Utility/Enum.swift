//
//  Enum.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/15/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

//MARK: State of home cell enum
enum CellState
{
    case collapsed
    case expanded
}

extension CellState
{
    mutating func switchState(){
        switch self{
        case .collapsed:
            self = .expanded
        case .expanded:
            self = .collapsed
        }
    }
}


//MARK: Web services enum
enum APIPath {
    case category
    case hotnews
    case news
    case newsList
    case related
    case trends
    case trendsList
}

extension APIPath {
    var URLPath: String {
        switch self {
        case .category:
            return "\(BASE_API)/category"
        case .hotnews:
            return "\(BASE_API)/hotnews"
        case .news:
            return "\(BASE_API)/news/"
        case .newsList:
            return "\(BASE_API)/news/list/"
        case .related:
            return "\(BASE_API)/news/related"
        case .trends:
            return "\(BASE_API)/topic/trends"
        case .trendsList:
            return "\(BASE_API)/news/topic"
        }
    }
}



//MARK: Result type enum
enum Result<InfoCode,ErrorCode>{
    case success(InfoCode)
    case failure(ErrorCode)
}

//MARK: Image name enum
//Note: Remember to add value here everytime adding a new image into casset
/// Enum for image name
enum ImageName : String,CustomStringConvertible{
    case AppIcon
    case CollapseIcon
    case ExpandIcon
    case ScrollToTopIcon
    case CancelIcon
    case EnabledBookmarkIcon
    case DisabledBookmarkIcon
    case MenuIcon
    case BookmarkIcon
    case SettingIcon
    case SearchIcon
    case ShareIcon
    case BackIcon
    
    var description: String{
        switch self{
        case .AppIcon : return "tintm-place-holder"
        case .CollapseIcon : return "collapse-icon"
        case .ExpandIcon : return "expand-icon"
        case .ScrollToTopIcon : return "scroll-to-top-icon"
        case .CancelIcon : return "cancel-icon"
        case .EnabledBookmarkIcon : return "enabled-bookmark-icon"
        case .DisabledBookmarkIcon : return "disabled-bookmark-icon"
        case .MenuIcon : return "menu-icon"
        case .BookmarkIcon : return "bookmark-icon"
        case .SettingIcon : return "settings-icon"
        case .SearchIcon : return "search-icon"
        case .ShareIcon : return "share-icon"
        case .BackIcon : return "back-icon"
        }
    }
}

//MARK: Info and error code
enum InfoCode {
    case doneLoadingNews
    case bookmarked
    case mailSent
    case doneLoadMore
    case doneDeletingNews
    case loadMoreNews
}

enum ErrorCode : Error{
    case noInternetConnection
    case unknownError
    case mailFailed
}

//TODO: Migrate error code to app error
enum AppError: Error, CustomStringConvertible{
    case networkError(code: Int)
    case commonError(code: Int)
    case unknownError
    case mailFailed
    
    var description: String {
        switch self {
        case .networkError(code: -1009):
            return "Xảy ra lỗi kết nối"
        case .commonError(code: 400):
            return "Co loi"
        case .unknownError:
            return "Không biết là lỗi gì, nhưng mà nói chung là có lỗi đó"
        case .mailFailed:
            return "Có lỗi khi gửi mail"
        default:
            return ""
        }
    }
}
