//
//  AlertView.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/9/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation

import NVActivityIndicatorView

struct AlertViewManager {
    let activityIndicator : NVActivityIndicatorView
    
    static let sharedInstance = AlertViewManager()
    
    fileprivate init(){
        
        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 100, y: 200, width: 50, height: 50), type: .ballSpinFadeLoader, color: .white, padding: 0)
        activityIndicator.center = CGPoint(x: APP_WIDTH/2, y: APP_HEIGHT/2)
    }
    
    func showSuccess(_ code :InfoCode) {
        switch code {
        case .bookmarked:
            Drop.down("Đã lưu", state: .success)
        case .mailSent:
             Drop.down("Email đã được gửi", state: .success)
        case .doneLoadingNews:
            Drop.down("Đã load hết tin", state: .info)
        case .doneLoadMore:
            Drop.down("Đang tải thêm tin", state: .info, duration: 1.0)
        case .doneDeletingNews:
            Drop.down("Đã xoá hết tin", state: .info, duration: 1.0)
        case .loadMoreNews:
            Drop.down("Đang tải thêm tin", state: .info,duration: 0.75)
            
        }
    }
    
    func showFailure(_ code: ErrorCode) {
        switch code{
        case .noInternetConnection:
             Drop.down("Không có kết nối mạng", state: .error)
        case .unknownError:
             Drop.down("Không biết là lỗi gì, nhưng mà nói chung là có lỗi đó", state: .error)
        case .mailFailed:
             Drop.down("Có lỗi khi gửi mail", state: .error)
        }
    }
    
    //Show and hide loading indicator
    //vc : view controller to display indicator
    func showLoading(_ vc: UIViewController, color: UIColor = UIColor.white) {
        vc.view.addSubview(activityIndicator)
        activityIndicator.center = SCREEN_CENTER
        activityIndicator.color = color
        activityIndicator.startAnimating()
    }
    
    func stopLoading(){
        activityIndicator.stopAnimating()
    }
}
