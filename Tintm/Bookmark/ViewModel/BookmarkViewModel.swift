//
//  BookmarkViewModel.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/28/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import Foundation
import RealmSwift

struct BookmarkViewModel {
    let realmManager = RealmCommandManager()
    var dataArray : [RealmHomeObject]{
        get {
            let temp : [RealmHomeObject ] =  realmManager.read(.home, dictionary: ["isBookmark":true as AnyObject]) as! [RealmHomeObject]
            return temp.sorted(by: {$0.0.timestamp.toDate().compare($0.1.timestamp.toDate() as Date) == .orderedDescending})
            //return temp
        }
    }
    
}

extension BookmarkViewModel : TextPresentable {
}
