//
//  BookmarkViewController.swift
//  Tintm
//
//  Created by Nguyen Luong on 7/28/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

import UIKit
import Google

class BookmarkViewController: UITableViewController, ButtonDelegate{

    var viewModel : BookmarkViewModel?
    var router : BookmarkViewRouter?
    
    override init(style: UITableViewStyle) {
        super.init(style: style)
        
        tableView.register(BookmarkCell.self, forCellReuseIdentifier: BOOKMARK_CELL_ID)
        //tableView.registerClass(HomeCellTypeOne.self, forCellReuseIdentifier: HOME_CELL_TYPE_ONE_ID)
        tableView.showsVerticalScrollIndicator = false
        tableView.alwaysBounceVertical = false
        
        tableView.estimatedRowHeight = APP_HEIGHT/5
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.setNeedsLayout()
        
        
    }
    
    convenience init(){
        self.init(style: .plain)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
        navigationController?.isNavigationBarHidden = false
        
        if viewModel?.dataArray.count == 0 {
            tableView.backgroundView = StateView(frame: CGRect(x: 0, y: 0, width: APP_WIDTH, height: APP_HEIGHT), state: .empty)
            tableView.separatorColor = UIColor.clear
        }
    }
    
   
    
    
    
    //MARK: Delegate and data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel!.dataArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCellWithIdentifier(HOME_CELL_TYPE_ONE_ID) as! HomeCellTypeOne
        let cell = tableView.dequeueReusableCell(withIdentifier: BOOKMARK_CELL_ID) as! BookmarkCell
        let object  = viewModel!.dataArray[indexPath.row]
        
        cell.publishData(object)
        cell.constrainSubview()
        cell.delegate = self
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.pushFromBookmarkViewToArticleView(indexPath.row)
        
        //Analytics
        let tracker = GAI.sharedInstance().defaultTracker
        let event = GAIDictionaryBuilder.createEvent(
            withCategory: "bookmark_event",
            action: "tapped",
            label: "bookmark_cell",
            value: nil).build()
        tracker?.send(event as! [AnyHashable: Any])
    }
    
    //MARK: Delegate method for button
    func didTapButtonInsideCell(_ sender: UITableViewCell) {
        let cell = sender as! BookmarkCell
        tableView.beginUpdates()
        switch cell.state{
        case .expanded:
            cell.collapseView()
        case .collapsed:
            cell.expandView()
            
        }
        tableView.layoutIfNeeded()
        tableView.endUpdates()
    }
    
    
}
